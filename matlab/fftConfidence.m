function [A, M, O] = fftConfidence(img, bs, na)
I = img;
%fftfun = @(x) 1 + log(abs(fftshift(fft2(x))));
%B = blockproc(I, [bs, bs], @(x) myfft(x.data));
[H, W] = size(I);
O = blockproc(img, [bs, bs], @(x) findAngles(x.data, na));
O = reshape(O, H/bs, na, W/bs);
O = medfilt1(O, 7, [], 2);
[M, O] = max(O, [], 2);
M = reshape(M, H/bs, W/bs);

O = pi * reshape(O, H/bs, W/bs) ./ na;

[h, w] = size(O);
A = zeros(size(I));
for i=1:h
    for j=1:w
        xc = (j-1)*bs + ceil(bs/2);
        yc = (i-1)*bs + ceil(bs/2);
        theta = O(i,j);
        f = @(x) tan(theta)*(x-xc) + yc;
        g = @(y) (y-yc)/tan(theta) + xc;
        
        a = tan(theta);
        %b = row - tan(theta) * col;
        
        X = [0, 0, 0, 0];
        Y = [0, 0, 0, 0];
        
        % Intersect : y = 1
        if a ~= 0
            X(1) = g(1);
            Y(1) = 1;
        end
        % Intersect : x = 1
        if a < 1e16
            X(2) = 1;
            Y(2) = f(1);
        end
        % Intersect : y = h
        if a ~= 0
            X(3) = g(H);
            Y(3) = H;
        end
        if a < 1e16
            % Intersect : x = w
            X(4) = W;
            Y(4) = f(W);
        end
        
        % Get only (X,Y) in image bounds
        PX = (X >= 1 & X <= W);
        PY = (Y >= 1 & Y <= H);
        X = X(PX & PY);
        Y = Y(PX & PY);
        
        if length(X) < 2 ||length(Y) < 2
            continue
        end
        
        sx = X(1);
        sy = Y(1);
        ex = X(2);
        ey = Y(2);
        % Get line pixels coordinates
        
        [lx, ly] = bresenham(sx, sy, ex, ey);
        
        idxs = sub2ind(size(A), ly, lx);
        
        A(idxs) = A(idxs) + 1;
        %imagesc(A), hold on;
        %scatter(xc, yc, 32, 'r', 'filled');
        %pause(0.01);
    end
end
%O = reshape(O, 48, 15, 64);
%C = zeros(size(img));
X = find(A>1);
I = sum(X .* A(X), 'all') ./ sum(A(X), 'all');
%[~, I] = max(A(:));
[y, x] = ind2sub(size(A), I);
imagesc(A), hold on, scatter(x, y, 32, 'r', 'filled');
end

function Y = myfft(x)
Y = log(1 + abs(fftshift(fft2(x))));
%Y = imgaussfilt(Y, 1);
%m = max(Y(:));
%Y(Y>0.95*m) = 0;
%Y(size(Y,1)/2+1, size(Y,2)/2+1) = 0;
end

function YY = findAngles(bloc, nbins)
X = log(1 + abs(fftshift(fft2(bloc))));
[r, c] = size(X);
xc = ceil(c/2);
yc = ceil(r/2);

angles = linspace(0, pi, nbins+1);
angles(end) = [];
old_K = -1;
Y = 0;
YY = zeros(1, nbins);
%figure(1), imagesc(X), hold on;
for i=1:length(angles)
    % line equation
    % y = tan(theta)(x-a) + b
    f = @(x) tan(angles(i))*(x-xc) + yc;
    g = @(y) (y-yc)/tan(angles(i)) + xc;
    
    if angles(i) >= 0 && angles(i) <= pi/4
        sx = 1;
        sy = f(1);
    end
    if angles(i) > pi/4 && angles(i) <= 3*pi/4
        sx = g(1);
        sy = 1;
    end
    if angles(i) > 3*pi/4 && angles(i) <= 5*pi/4
        sx = c;
        sy = f(c);
    end
    
    ex = floor(c/2)+1;
    ey = floor(r/2)+1;
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    idxs = sub2ind(size(X), ly, lx);
    new_K = sum(X(idxs)) ./ length(idxs);
    
%     if new_K > old_K
%         old_K = new_K;
%         Y = angles(i);
%     end
    YY(i) = new_K;
    %figure(1), scatter(lx, ly, 36, 'b', 'filled');
    %pause(0.1);
end
%Y = max(YY);
%figure(2), plot(1:6:180, Y);
%pause();
end
