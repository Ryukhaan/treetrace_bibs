lx = 32;
ly = 24;
list = cell(1, lx*ly);
for i=1:ly
    for j=1:lx
        %subplot(ly, lx, (i-1)*lx+j);
        plot(1:2:180, reshape(O(i,:,j), 1, 90));
        ff = getframe;
        list{1, (i-1)*lx+j} = ff.cdata;
    end
end
montage(list, 'Size', [ly, lx]);