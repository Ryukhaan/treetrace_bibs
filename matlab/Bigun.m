function A = Bigun(img, npyms, sigmap, beta, w, bw, Tc, sigmai, Ts)
Ep = cell(1, npyms);
Lp = cell(1, npyms);
gradf = cell(1,npyms);
theta = cell(1,npyms);
I = double(img);
[X, Y] = meshgrid(1:w, 1:w);
X = X - ceil(w/2);
Y = Y - ceil(w/2);
hx = exp(-beta * Y.^2) .* (X .* exp(-beta * X.^2));
hy = exp(-beta * X.^2) .* (Y .* exp(-beta * Y.^2));
hg = fspecial('gaussian', [17, 17], 1.0);

for i=1:npyms
    F = imgaussfilt(I, sigmap);
    G = imgaussfilt(I, 2*sigmap);
    Lp{i} = G - F;
    I = imresize(Lp{i}, 0.5);
    %Ep{i} = imgaussfilt(Lp{i}.^2, 4);
    gradf{i} = complex(imfilter(Lp{i}, hx), imfilter(Lp{i}, hy));
    squared =  gradf{i} .^ 2;
    theta{i} = imfilter(squared, hg) ./ imfilter(abs(squared), hg);
end

idx = 2;
[h, w] = size(Lp{idx});
[H, W] = size(Lp{idx});
A = zeros(size(Lp{idx}));
Oi = angle(theta{idx});
Oi = imgaussfilt(Oi, 1.0);
Ci = abs(theta{idx});
%bw = 20;
%Ts = 0.96;
for i=1:bw:h
    for j=1:bw:w
        ww = Ci(i:i+bw-1, j:j+bw-1);
        oo = Oi(i:i+bw-1, j:j+bw-1);
        [m, idx] = max(ww(:));
        if m < Tc || m > 0.98 || isnan(m)
            continue
        end
        orientation = oo(idx);
        if isnan(orientation)
            continue
        end
        xc = j + ceil(bw/2);
        yc = i + ceil(bw/2);
        
        a = tan(orientation);
        f = @(x) a*(x-xc) + yc;
        g = @(y) (y-yc)/a + xc;
        
        %b = row - tan(theta) * col;
        
        X = [0, 0, 0, 0];
        Y = [0, 0, 0, 0];
        
        % Intersect : y = 1
        if a ~= 0
            X(1) = g(1);
            Y(1) = 1;
        end
        % Intersect : x = 1
        if a < 1e16
            X(2) = 1;
            Y(2) = f(1);
        end
        % Intersect : y = h
        if a ~= 0
            X(3) = g(H);
            Y(3) = H;
        end
        if a < 1e16
            % Intersect : x = w
            X(4) = W;
            Y(4) = f(W);
        end
        
        % Get only (X,Y) in image bounds
        PX = (X >= 1 & X <= W);
        PY = (Y >= 1 & Y <= H);
        X = X(PX & PY);
        Y = Y(PX & PY);
        
        if length(X) < 2 ||length(Y) < 2
            continue
        end
        
        sx = X(1);
        sy = Y(1);
        ex = X(2);
        ey = Y(2);
        % Get line pixels coordinates
        
        [lx, ly] = bresenham(sx, sy, ex, ey);
        
        idxs = sub2ind(size(A), ly, lx);
        
        A(idxs) = A(idxs) + 1;
        %imagesc(A);
        %scatter(xc, yc, 32, 'r', 'filled');
        %pause(0.1);
    end
end
A = imgaussfilt(A, sigmai);
s1 = std(A, [], 'all')
m  = max(A(:));
idx = find(A>Ts*m);
%idm = mean(idx(:));
[y, x] = ind2sub(size(A), idx);
ym = ceil(mean(y, 'all'));
xm = ceil(mean(x, 'all'));
%imagesc(log(1+A)), hold on, scatter(xm,ym,32,'r','filled');
%imagesc(Lp{2}), colormap gray, hold on, scatter(xm,ym,32,'r','filled');

idx = 1;
[h, w] = size(Lp{idx});
[H, W] = size(Lp{idx});
A = zeros(size(Lp{idx}));
Oi = angle(theta{idx});
%Oi = imgaussfilt(Oi, 1.0);
Ci = abs(theta{idx});

b=32;
sy = max(1, 2*ym-512);
ey = min(H, 2*ym+512);
sx = max(1, 2*xm-512);
ex = min(W, 2*xm+512);
imagesc(Ci), hold on;
scatter(sx, sy, 32, 'r', 'filled');
scatter(ex, ey, 32, 'r', 'filled');

for i=sy:b:ey-b
    for j=sx:b:ex-b
        ww = Ci(i:i+b-1, j:j+b-1);
        oo = Oi(i:i+b-1, j:j+b-1);
        [m, idx] = max(ww(:));
        if m < Tc || m > 0.98 || isnan(m)
            continue
        end
        orientation = oo(idx);
        if isnan(orientation)
            continue
        end
        xc = j + ceil(b/2);
        yc = i + ceil(b/2);
        pause(0.1);
        
        a = tan(orientation);
        f = @(x) a*(x-xc) + yc;
        g = @(y) (y-yc)/a + xc;
        
        %b = row - tan(theta) * col;
        
        X = [0, 0, 0, 0];
        Y = [0, 0, 0, 0];
        
        % Intersect : y = 1
        if a ~= 0
            X(1) = g(1);
            Y(1) = 1;
        end
        % Intersect : x = 1
        if a < 1e16
            X(2) = 1;
            Y(2) = f(1);
        end
        % Intersect : y = h
        if a ~= 0
            X(3) = g(H);
            Y(3) = H;
        end
        if a < 1e16
            % Intersect : x = w
            X(4) = W;
            Y(4) = f(W);
        end
        
        % Get only (X,Y) in image bounds
        PX = (X >= 1 & X <= W);
        PY = (Y >= 1 & Y <= H);
        X = X(PX & PY);
        Y = Y(PX & PY);
        
        if length(X) < 2 ||length(Y) < 2
            continue
        end
        
        start_x = X(1);
        start_y = Y(1);
        end_x = X(2);
        end_y = Y(2);
        % Get line pixels coordinates
        
        [lx, ly] = bresenham(start_x, start_y, end_x, end_y);
        
        idxs = sub2ind(size(A), ly, lx);
        
        A(idxs) = A(idxs) + 1;
        %imagesc(log(1+A));
         scatter(xc, yc, 8, 'r', 'filled');
%         pause(0.1);
    end
end
A = imgaussfilt(A, sigmai-1);
s1 = std(A, [], 'all')
m  = max(A(:));
idx = find(A>Ts*m);
%idm = mean(idx(:));
[y, x] = ind2sub(size(A), idx);
ym = mean(y, 'all');
xm = mean(x, 'all');
%imagesc(log(1+A)), hold on, scatter(xm,ym,32,'r','filled');
imagesc(Lp{1}), colormap gray, hold on, scatter(xm,ym,32,'r','filled');

end