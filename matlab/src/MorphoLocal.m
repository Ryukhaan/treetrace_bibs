function [A, O, C] = MorphoLocal(img, rois)
I = img;
if length(size(I)) == 3
    I = rgb2gray(I);
end

w = fspecial('gaussian', 5, 1.0);
J = imfilter(I, w);
%I = medfilt2(I, [3, 3]);

% Sobel Operator
[~, ~, Gv, Gh] = edge(J, 'Sobel');
% N = sqrt(Gv.^2 + Gh.^2);
% fh = [
%     -0.112737, 0, 0.112737;
%     -0.274526, 0, 0.274526;
%     -0.112737, 0, 0.112737];
% fv = fh';
% 
% Gh = double(imfilter(I, fh, 'replicate', 'same'));
% Gv = double(imfilter(I, fv, 'replicate', 'same'));
N = sqrt(Gv .* Gv + Gh .* Gh);

O = atan2(Gv, Gh);

nbins = 86;
O = 255 .* (O - min(O(:))) / (max(O(:)) - min(O(:)));
%thresh = multithresh(O,nbins-1);
O = uint8(floor(double(O)/3)+1);
%O = imquantize(O, nbins);
%O = O+1;
%O(O==nbins+1)=1;

F = zeros(size(O,1), size(O,2), 2);
F(:,:,1) = N;
F(:,:,2) = O;
tmp = blockproc(F, [rois rois], @(x) SingleOrient(x.data, nbins));
% for i=1:rois:size(O,1)
%     for j=1:rois:size(O,2)
%         winN = N(i:i+rois-1, j:j+rois-1);
%         winO = O(i:i+rois-1, j:j+rois-1);
%         new = ModeOrient(winN, winO, nbins);
%         O(i:i+rois-1, j:j+rois-1) = new;
%     end
% end
O = imresize(tmp, size(I), 'nearest');

C = blockproc(N, [rois, rois], @(x) std(x.data, 0, 'all'));
M = blockproc(N, [rois, rois], @(x) mean(x.data, 'all'));

[h, w] = size(I);
G = I;
for r=1:rois:h
    for c=1:rois:w
    lo = O(r, c);
    %switch mod(lo-1,4)
%     switch lo-1
%     case 0
%         neigh = zeros(7);
%         neigh(:, 4) = 1;
% %         neigh = [ ...
% %             0 0 1;
% %             0 1 1;
% %             0 0 1];
%     case 1
%         neigh = eye(7);
% %         neigh = [ ...
% %             0 1 1;
% %             0 1 1;
% %             0 0 0];
%     case 2
%         neigh = zeros(7);
%         neigh(4,:)=1;
% %         neigh = [ ...
% %             1 1 1;
% %             0 1 0;
% %             0 0 0];
%     case 3
%         neigh = fliplr(eye(7));
%         neigh = [ ...
%             1 1 0;
%             1 1 0;
%             0 0 0];
%     case 4
%         neigh = [ ...
%             1 0 0;
%             1 1 0;
%             1 0 0];
%     case 5
%         neigh = [ ...
%             0 0 0;
%             1 1 0;
%             1 1 0];
%     case 6
%         neigh = [ ...
%             0 0 0;
%             0 1 0;
%             1 1 1];
%     case 7
%         neigh = [ ...
%             0 0 0;
%             0 1 1;
%             0 1 1];
%    end
    %neigh = [0 1 1; 0 1 0; 1 1 0];
    se = strel('line', 8, lo * 11.25);
    %se = strel('arbitrary', neigh);
    G(r:r+rois-1,c:c+rois-1) = imopen(imopen(G(r:r+rois-1,c:c+rois-1), se), se);
    end
end
A =G;
%A = blockproc(N, [rois rois], @(x) ErodeDirectional(x.data, O, G));
%A = blockproc(I, [rois, rois], @(x) LocalStrel(x.data, O));
end


function Z = ModeOrient(X, Y, nbins)
%counter = ones(1, nbins);
%gradmag = zeros(1, nbins);
somme = zeros(1, nbins);
[h, w] = size(X);
n = h * w;
for i=1:n
    idx = Y(i);
    somme = somme + circshift(gausswin(nbins), idx-nbins/2)';
    %counter(idx) = counter(idx) + 1;
    %gradmag(idx) = gradmag(idx) + X(i);
end
%avg = gradmag ./ counter;
avg = somme ./ n;
[~, no] = max(avg(:));
Z = no .* ones(size(X));
end

function Z = SingleOrient(X, nbins)
%counter = ones(1, nbins);
%gradmag = zeros(1, nbins);
somme = zeros(1, nbins);
[h, w, ~] = size(X);
for i=1:h
    for j=1:w
        idx = X(i, j, 2);
        %somme = somme + circshift(gausswin(nbins), idx-nbins/2)';
        somme(idx) = somme(idx) + 4;
        if idx-1<=0
            somme(nbins) = somme(nbins) + 1;
        else
            somme(idx-1) = somme(idx-1) + 1;
        end
        if idx+1>nbins
            somme(1) = somme(1) + 1;
        else
            somme(idx+1) = somme(idx+1) + 1;
        end
        %counter(idx) = counter(idx) + 1;
        %gradmag(idx) = gradmag(idx) + X(i, j, 1);
    end
end
%avg = gradmag ./ counter;
%avg = somme ./ (h*w);
avg = somme;
[~, no] = max(avg(:));
Z = no .* ones(h, w);
end

