function aabb = MeanBox(I)
m = mean(I, 'all');
B = I;
B(B<m)=0;
B(B>=m)=1;
[~, B] = bwboundaries(B);

mx = floor(size(B,2)/2);
my = floor(size(B,1)/2);
C = B(my-127:my+127, mx-127:mx+127);
idxs = C(randi(numel(C), 20, 1));
idx = mode(idxs);
%idx = B(size(B,1)/2, size(B,2)/2);
B(B~=idx)=0;
B(B==idx)=1;
[row, col] = find(B);
aabb = [min(row), max(row), min(col), max(col)];
end