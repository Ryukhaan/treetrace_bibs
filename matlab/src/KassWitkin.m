function [N, O, C] = KassWitkin(I, ws)
[~, ~, Gv, Gh] = edge(I, 'Sobel');
N = sqrt(Gv .* Gv + Gh .* Gh);
%O = atan(Gv ./ Gh);
%r = Gv .* Gv + Gh .* Gh;

Gxx = Gh .^ 2;
Gyy = Gv .^ 2;
Gxy = Gv .* Gh;
sGxx = blockproc(Gxx, [ws, ws], @(x) sum(x.data, 'all'));
sGyy = blockproc(Gyy, [ws, ws], @(x) sum(x.data, 'all'));
sGxy = blockproc(Gxy, [ws, ws], @(x) sum(x.data, 'all'));
O = pi/2 + 0.5 * atan2( 2 * sGxy, sGxx - sGyy);

% Dx = r .* cos(2 .* O);
% Dy = r .* sin(2 .* O);
% Ax = blockproc(Dx, [ws, ws], @(x) sum(x.data, 'all') ./ (ws .^ 2));
% Ay = blockproc(Dy, [ws, ws], @(x) sum(x.data, 'all') ./ (ws .^ 2));

C = sqrt((sGxx - sGyy).^2 + 4*sGxy.^2)  ./ (sGxx + sGyy);

end