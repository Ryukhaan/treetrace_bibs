function Pi = PoincareIndex(O)
Pi = zeros(size(O)-2);
[h, w] = size(O);
for row=2:h-1
    for col=2:w-1
        d1 = O(row, col-1);
        d2 = O(row-1, col-1);
        d3 = O(row-1, col);
        d4 = O(row-1, col+1);
        d5 = O(row, col+1);
        d6 = O(row+1, col+1);
        d7 = O(row+1, col);
        d8 = O(row+1, col-1);
        D1 = [d1, d2, d3, d4, d5, d6, d7, d8];
        D2 = [d2, d3, d4, d5, d6, d7, d8, d1];
        Pi(row-1,col-1)= sum(D2-D1, 'all');
    end
end
end