function A = Kirsch(I)
% I grayscale image
p = 0.274526;
n = -0.112737;

h1 = [p, p, p; n, 0, n; n, n, n];
h2 = [p, p, n; p, 0, n; n, n, n];
h3 = [p, n, n; p, 0, n; p, n, n];
h4 = [n, n, n; p, 0, n; p, p, n];
hp = [n, n, n; n, 0, n; p, p, p];
h6 = [n, n, n; n, 0, p; n, p, p];
h7 = [n, n, p; n, 0, p; n, n, p];
h8 = [n, p, p; n, 0, p; n, n, n];

A = zeros([size(I), 8]);
A(:,:,1) = imfilter(I, h1);
A(:,:,2) = imfilter(I, h2);
A(:,:,3) = imfilter(I, h3);
A(:,:,4) = imfilter(I, h4);
A(:,:,5) = imfilter(I, hp);
A(:,:,6) = imfilter(I, h6);
A(:,:,7) = imfilter(I, h7);
A(:,:,8) = imfilter(I, h8);
A = max(A, [], 3);

end
