% Run

% Open files directory
files = dir('/Users/VoidMain/Desktop/Doctorat/treetrace/dataset/selected/*.jpg');

% Output file
tab = strings(length(files)+1, 3);
tab(1,:) = ["Name", "X", "Y"];
%tab = [tab; ["Name", "X", "Y"]];

timer = 0;
% Iterate through files
i = 0;
for file=files'
    % Read file
    image = imread([file.folder, '/', file.name]);
    [H, W, ~] = size(image);
    
    % Grayscale and Resizing
    hsv = rgb2hsv(image);
    %treated = imresize(treated, 0.25);
    hsv(:,:,2) = 255;
    hsv(:,:,3) = 255;
    m = mean(hsv(:,:,1), 'all');
    C = hsv(:,:,1);
    C(C<m)=0;
    C(C>=m)=1;
    C = 1 - C;
    treated = rgb2gray(image .* uint8(C));
    
    treated = MorphoLocal(treated, 32);
    %treated = fftConfidence(treated, 62);
    tic;
    imagesc(treated);
    toc;
    new_width = 960;
    new_height = 720;
    %treated = imresize(treated, [new_height, new_width]);
    scalex = W / new_width;
    scaley = H / new_height;
    
    % Focus on Pitch Bounding Box
    % aabb = [min(y), max(y), min(x), max(x)]
    %aabb = MeanBox(treated);
    %aabb = HoughBox(treated);
    %treated=treated(aabb(1):aabb(2), aabb(3):aabb(4));
    %imwrite(treated, ['./BoundingBox/MeanMode/MeanMode_', file.name]);
    %treated = Norell(treated);
    %treated = medfilt2(treated, [3,3]);
    
    % Get scale
%     offsetx = aabb(3);
%     offsety = aabb(1);
%     [th, tw] = size(treated);
    %treated = blockproc(treated, [12, 12], @(x) Jain(x.data));
    
    % Gradient Computation
    %N = Kirsch(treated);
    %[N, O] = Sobel(treated);
    %[N, O] = Hast(treated);
    %[N, O] = Optimum3x3(treated);
    %[N, O] = Bo(treated, 32, 0.0375);
    %[N, O] = Conner(treated, 8);
    %N = Gabor(treated);
    %[E, N, O] = ConnerOptimum(treated, 12);
 
    %Gradient
    %imagesc(N); colormap gray;
    %N;
    %pause(5);
    
    % Orientation Computation
    %O = blocOrient(treated, 12);
    %N = blockproc(treated, [12, 12], @(x) edge(x.data, 'Sobel'));
    
    % Gradient Post Treatment
    %N = medfilt2(N, [3, 3]);
    
    % Debug
    %subplot(1,2,1);
    %imshow(treated);
    %subplot(1,2,2);
    %imagesc(treated); colormap gray;
    %length(find(N))
    %pause(2);
    
    % Normalize
    %N = uint8(255 .* (N - min(N(:))) / (max(N(:)) - min(N(:))));
    %[counts, ~] = imhist(N(:), 256);
    %     dstb = fitdist(N(:), 'HalfNormal');
    %     O = (O - min(O(:))) / (max(O(:)) - min(O(:)));
    
    
    % Save norm
    % imwrite(N, ['./Filters/Kirsch/Optimum/Kirsch_', file.name]);
    %     imwrite(O, ['./Filters/Optimum3x3/Orientation/Optimum3x3_', file.name]);
    
    % Threshhold Gradient Magnitude
    %[counts,x] = imhist(N, 255);
    %T = otsuthresh(counts);
    %T = triangle_th(counts, 255);
    %N = logical(imbinarize(N, T));
   
    %N = bwskel(N);
    %    imagesc(N);
    %N;
    %    imagesc(N); hold on;
    %     Mean
    %     m = mean(N, 'all');
    %     N(N<m) = 0;
    %     N(N>=m) = 1;
    
    %      Fixed
    %     N(N<60)=0;
    %     N(N>=60)=1;
    %     if length(find(N)) > 30000
    %     length(find(N))
    %         continue;
    %     end
    
    % 3sigma
    %      T1 = prctile(N(:),68);
    %      %T2 = prctile(N(:),95);
    %      T1 = dstb.sigma;
    %
    %      N(N<T1)=0;
    %      N(N>2*T1)=0;
    %      N(N~=0)=1;
    % Pith detection
    tic;
    %A = Longuetaud(N, O);
    %treated = imresize(treated, [1080, 1440], 'nearest');
    %A = HOGs(treated, 12, 3, 10);
    timer = timer + toc;
    toc;
    
    
    % Barycentre (like Entacher et al. 2007)
    %     [row, col] = find(A>1);
%     idx = find(A>1);
%     [X, Y] = meshgrid(1:tw, 1:th);
%     mc = sum(double(A(idx)) .* X(idx), 'all') / sum(double(A(idx)), 'all');
%     mr = sum(double(A(idx)) .* Y(idx), 'all') / sum(double(A(idx)), 'all');
    
    
    % Take only max
    % [rows, cols] = find(A==max(A(:)));
    [~, I] = max(A(:));
    [rows, cols] = ind2sub(size(A), I);
    mc = mean(cols(:));
    mr = mean(rows(:));
    
    % Back to image dimension
    %mc = scalex * (mc + offsetx);
    %mr = scaley * (mr + offsety);
    
    %length(find(E))
    figure(1);
    imagesc(A); hold on; colormap hot;
    %montage({N, O}); hold on;
    scatter(mc, mr, 64, 'b', 'filled');
    pause(0.01);
    
    i = i+1
    tab(i+1, :) = [string(file.name), num2str(mc), num2str(mr)];
end

timer / length(files)

% Write CSV file
fid = fopen('pith_location.csv','wt');
if fid>0
    fprintf(fid,'%s,%s,%s\n',tab(1,:));
    for k=2:size(tab,1)
        fprintf(fid,'%s,%f,%f\n',tab(k,:));
    end
    fclose(fid);
end