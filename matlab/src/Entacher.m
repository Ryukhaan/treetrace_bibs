function ires = Entacher(img, ws)
% Towards an Automated Generation of Tree Ring Profiles from CT-Images
% Entachr et al. 2008

% Parameters :
% img : An RGB image of size 3240x4320
% ws  : Laplacian of Gaussian window size

if length(size(img)) == 3
    ires = rgb2gray(img);
else
    ires = img;
end
ires = imresize(ires, [1080, 1440]);

w = fspecial('gaussian', 3, 1.0);
ires = imfilter(ires, w);
%ires = imgaussfilt(ires, 3.0);
%ires = adapthisteq(ires);

% LOG
w = fspecial('log', [ws ws], 0.5); 
ires = imfilter(ires, w, 'replicate'); 

% 50% cut-off
T = median(ires(ires~=0), 'all');
ires(ires<T) = 0;
ires(ires>=T) = 1;

%N = ires;
%N = bwmorph(ires,'skel',Inf);
ires = skeleton(ires);
end