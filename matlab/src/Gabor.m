function Y = Gabor(X)
orientation = linspace(0, 180, 10);
fi = 2;
g = gabor(fi,orientation);
[M, ~] = imgaborfilt(X, g);
outSize = size(M);

for i=1:outSize(3)
    M(:,:,i) = blockproc(M(:,:,i), [16, 16], @(x) imbinarize(x.data, mean(x.data, 'all')));
end
Y = logical(mode(M, 3));
%Y = bwskel(Y);
%Y = M;
end