files = dir('../../../cnnenv/dataset/target/*.jpg');

for file=files'
    I = imread([file.folder, '/', file.name]);
    [~, idx] = max(I(:));
    %I(~idx) = 0;
    I(idx) = 1e9;
    I(I~=1e9) = 0;
    imshow(I);
    pause(1);
    %imwrite(I, [file.folder, '/', file.name]);
end