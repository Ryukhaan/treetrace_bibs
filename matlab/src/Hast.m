function [N, O] = Hast(J)

M = [1,-3,3,-1; -1,4,-5,2; 0,1,0,-1; 0,0,2,0]*0.5;
u = [0.125;0.25;0.5;1];
up = [0.75;1;1;0];
d = up'*M;
k = u'*M;
dk = conv(d,k);
kk = conv(k,k);
Gv = conv2(dk, kk,J,'same');  % vertical derivative (wrt Y)
Gh = conv2(conv(k,k),conv(d,k),J,'same');  % horizontal derivative (wrt X)

% Get magnitude and orientation
N = sqrt(Gv.^2 + Gh.^2);
O = atan2(Gv, Gh);

end