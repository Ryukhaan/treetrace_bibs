function [R] = Jain(B, ws)
%     mo = mean(B, 'all');
%     vo = std(double(B), 0, 'all');
%     mp = m + sqrt(v / vo .* double((B-mo).^2));
%     mm = m - sqrt(v / vo .* double((B-mo).^2));
%     B(B<mo) = 0;
%     B(B>=mo) = 1;
%     E = double(1-B) .* mm + double(B) .* mp;
% [~,~, Gy, Gx] = edge(B, 'Sobel');
% 
% Gxx = blockproc(Gx, [ws, ws], @(x) sum(x.data.^2, 'all'));
% Gyy = blockproc(Gy, [ws, ws], @(x) sum(x.data.^2, 'all'));
% prod = Gy .* Gx;
% Gxy = blockproc(prod, [ws, ws], @(x) sum(x.data, 'all'));
% 
% C = sqrt((Gxx-Gyy).^2 + 4*Gxy.^2) ./ (Gxx + Gyy);
%R = blockproc(B, [ws,ws], @(x) log(1+abs(fftshift(fft2(x.data)))));

% prod = 2 * Gy .* Gx;
% diff = Gx.^2 - Gy.^2;
% summ = Gx.^2 + Gy.^2;
% prod = imgaussfilt(prod, 1);
% diff = imgaussfilt(diff, 1);
% summ = imgaussfilt(summ, 1);
% bp = blockproc(prod, [ws, ws], @(x) sum(x.data, 'all'));
% abp = blockproc(prod, [ws, ws], @(x) sum(abs(x.data), 'all'));
% 
% bd = blockproc(diff, [ws, ws], @(x) sum(x.data, 'all'));
% abd = blockproc(diff, [ws, ws], @(x) sum(abs(x.data), 'all')); 
% 
% bs = blockproc(summ, [ws, ws], @(x) sum(x.data, 'all'));
% 
% C = sqrt((bp.^2 + bd.^2) ./ ( bs));
% Cx = abs(bd) ./ abd;
% Cy = abs(bp) ./ abp;
%C = 0.5 * (Cx + Cy);
% O = 0.5 * atan2(bd, bp);
% O(O>0) = O(O>0) + pi/2;
% O(O<=0) = O(O<=0) - pi/2;
% 
% phix = cos(2 * E);
% phiy = sin(2 * E);
% 
% smoothx = imgaussfilt(phix, 1);
% smoothy = imgaussfilt(phiy, 1);
% E = 0.5 * atan2(smoothy, smoothx);

% [h, w] = size(B);

M = blockproc(B, [ws, ws], @(x) mean(x.data, 'all'));
S = blockproc(B, [ws, ws], @(x) std(double(x.data), 1, 'all'));
M(M==0) = -1;
C = S ./ M;
C(C<=0) = 0;
C = log(1+C);

%C = medfilt2(C, [3,3]);
%level = graythresh(C);
%BW = imbinarize(C, level);
% se = strel('square', 3);
% C = imopen(C, se);
% C = imclose(C, se);

%C = medfilt2(C, [3, 3]);
E = edge(C, 'Canny');
%E = 1BW;
D = bwdist(E);
D = -D;
D(E) = Inf;
L = watershed(D);
L(E) = 0;
LA = regionprops(L, 'Area');
LA = cat(1,LA.Area);
[~, i] = max(LA);
mask = imresize(L==i, size(B));
R = mask;% .* B;
%R = labeloverlay(C, L);
% imagesc(R);
% pause(0.5);
% L = zeros(1, h/ws * w/ws);
% U = zeros(1, h/ws * w/ws);
% acc = 1;
% for i=1:ws:h-1
%     for j=1:ws:w-1
%         W = B(i:i+ws-1, j:j+ws-1);
%         m = mean(W(:));
%         U(acc) = m + 1 / length(find(W>m)) * sum( (W(W>m) + m).^2, 'all');
%         L(acc) = m - 1 / length(find(W>m)) * sum( (W(W>m) - m).^2, 'all');
%         acc = acc + 1;
%     end
% end
% U = reshape(U, [h/ws, w/ws]);
% L = reshape(L, [h/ws, w/ws]);
end