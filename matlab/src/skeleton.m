function Y = skeleton(X)
[h, w] = size(X);
Y = zeros(size(X));
X_new = ones(size(X)+2);
X_new(2:h+1,2:w+1) = 1-X;
marked = zeros(size(X_new));
ndelete = Inf;
while ndelete ~= 0
    ndelete = 0;
    for j=2:h+1
        for i=2:w+1
            B = X_new(j,i)==0;
            if ~B; continue; end
            W = X_new(j-1:j+1, i-1:i+1);
            % At least one pixel is background (white)
            UP = [X_new(j,i-1), X_new(j-1,i), X_new(j,i-1)];
            white1 = length(find(UP)) >= 1;
            if ~white1; continue; end
            % At least one pixel is background (white)
            DOWN = [X_new(j-1,i), X_new(j+1,i), X_new(j,i-1)];
            white2 = length(find(DOWN)) >= 1;
            if ~white2; continue; end
           
            % Connectivity == 1
            C = connectivity(W) == 1;
            if ~C; continue; end
            % At least two white neighbors no more tlan 6
            neighbor = length(find(W==0));
            N = neighbor >= 2 && neighbor < 6;
            if ~N; continue; end
            marked(j, i) = 1;
            ndelete = ndelete + 1;
        end
    end
    %X_old = X_new;
    X_new(marked==1) = 1;
    marked = zeros(size(X_new));
    for j=2:h+1
        for i=2:w+1
            B = X_new(j, i) == 0;
            if ~B
                continue
            end
            
            % At least one pixel is background (black)
            UP = [X_new(j-1,i), X_new(j,i+1), X_new(j+1,i)];
            white1 = length(find(UP)) >= 1;
            if ~white1
                continue
            end
            
            % At least one pixel is background (black)
            DOWN = [X_new(j,i+1), X_new(j+1,i), X_new(j,i-1)];
            white2 = length(find(DOWN)) >= 1;
            if ~white2
                continue
            end
            
            % Connectivity == 1
            C = connectivity(W) == 1;
            if ~C
                continue
            end
            % At least two white neighbors no more tlan 6
            neighbor = length(find(W==0));
            N = neighbor >= 2 && neighbor < 6;
            if ~N
                continue
            end
            marked(j, i) = 1;
            ndelete = ndelete + 1;
        end
    end
    X_new(marked==1) = 1;
    imshow(X_new);
    pause(1);
end
end


function C = connectivity(W)
C = 0;
% S = {1,3,5,7}
for k=1:2:7
    C = C + W(convert(k)) - W(convert(k))*W(convert(k+1))*W(convert(k+2));
end
end


function [nk] = convert(k)
nk = 5; 
switch mod(k, 8)
    case 1
        nk=8;
    case 2
        nk=7;
    case 3
        nk=4;
    case 4
        nk=1;
    case 5
        nk=2;
    case 6
        nk=3;
    case 7
        nk=6;
    case 0
        nk=9;
end
end