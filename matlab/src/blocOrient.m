function O = blocOrient(I, ws)
[~, ~, Gv, Gh] = edge(I, 'Sobel');
J1 = 2 .* Gv .* Gh;
J2 = Gv .* Gv - Gh .* Gh;
sJ1 = blockproc(J1, [ws, ws], @(x) sum(x.data, 'all'));
sJ2 = blockproc(J2, [ws, ws], @(x) sum(x.data, 'all'));
O = 0.5 .* atan2(sJ1, sJ2);
O = imresize(O, size(I), 'bicubic');
end