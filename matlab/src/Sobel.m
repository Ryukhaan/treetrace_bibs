function [N, O] = Sobel(I)
% Sobel Filter
% Parameters 
% ----------
% I : RGB image
%
% Returns
% -------
% N : Magnitude Gradient
% O : Orientation
%

[~, ~, Gv, Gh] = edge(I, 'Sobel');
N = sqrt(Gv .* Gv + Gh .* Gh);
O = atan2(Gh, Gv);
end