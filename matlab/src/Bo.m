function [N, O, C, Phixx, Phiyy, F] = Bo(img, ws, gt)
% Fingerprint Singular Point Detection Algorithm by Poincar� Index
% Bo et al. 2008

% Assert : img is an image RGB with 3240x4320
if length(size(img)) == 3
    ires = rgb2gray(img);
else
    ires = img;
end
%ires = imresize(ires, [648, 864]);

w = fspecial('gaussian', 3, 1.0);
ires = imfilter(ires, w, 'replicate');

% fh = [
%     -0.112737, 0, 0.112737;
%     -0.274526, 0, 0.274526;
%     -0.112737, 0, 0.112737];
% fv = fh';
%Gh = double(imfilter(ires, fh));
%Gv = double(imfilter(ires, fv));
[~,~,Gv, Gh] = edge(ires, 'Sobel');
%[~, ~, Gv, Gh] = edge(ires, 'Sobel');
N = sqrt(Gv .* Gv + Gh .* Gh);
% m = min(N(:));
% M = max(N(:));
% Gt = gt .* (abs(M) - abs(m)) + abs(m);
% 
% N(N<Gt)=0;
% N(N>=Gt)=1;

J1 = 2 .* Gv .* Gh;
J2 = Gv .^ 2 - Gh .^ 2;
J3 = sqrt(Gv .^ 2 + Gh .^ 2);

sJ1 = blockproc(J1, [ws, ws], @(x) sum(x.data, 'all') ./ (ws.^2));
sJ2 = blockproc(J2, [ws, ws], @(x) sum(x.data, 'all') ./ (ws.^2));
O = 0.5 .* atan2 (sJ1, sJ2);
D = 0.5 .* atan2 (sJ2, sJ1);
%O = imresize(O, size(ires), 'nearest');

Phix = cos(2 .* D);
Phiy = sin(2 .* D);

% boxKernel = 1/25 .* ones(5,5); % Or whatever size window you want.
% Phixx = conv2(Phix, boxKernel, 'same');
% Phiyy = conv2(Phiy, boxKernel, 'same');
h = fspecial('average',[5, 5]);
Phixx = imfilter(Phix, h);
Phiyy = imfilter(Phiy, h);

F = 0.5 * atan2(Phiyy, Phixx);

sJ3 = blockproc(J3, [ws, ws], @(x) sum(x.data, 'all') / (ws .^ 2));
C = sqrt(sJ1 .^ 2 + sJ2 .^ 2) ./ sJ3;
% tmp = C;
% tmp(tmp<Gt.*Gt) = -1;
% idxs = tmp==-1;
% C = sqrt(sJ1 .^ 2 + sJ2 .^ 2) ./ sJ3;
C = (C - min(C(:)))/ (max(C(:)) - min(C(:)));
% C(idxs) = -1;

%N = 255 .* (N - min(N(:))) / (max(N(:)) - min(N(:)));
end