function [A] = Longuetaud(G, O)
% 
% Longuetaud et al. 2004
%
%
% We assume that Gradient, Orientation and Preprocessing have been done.
%
% Parameters
% ----------
% G : Gradient magnitude
% O : Orientation
%
%
% Returns
% -------
% A : Hough-like Accumulator
%

[h, w] = size(G);
%A = uint8(zeros(size(G)));
A = zeros(size(G));
[erows, ecols] = find(G);
n = length(erows)
% E = find(G);
% if length(E) < 5000
%     [erows, ecols] = find(G);
%     n = length(erows);
% else
%     idxs = E(randi(numel(E), 5000, 1));
%     [erows, ecols] = ind2sub(size(G), idxs);
%     n = length(erows);
% end
for i=1:n
    row = erows(i);
    col = ecols(i);
    
    theta = O(row, col);
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersect : y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersect : x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersect : y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    if a < 1e16
        % Intersect : x = w
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get only (X,Y) in image bounds
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    
    if length(X) < 2 ||length(Y) < 2
        continue
    end
        
    sx = X(1);
    sy = Y(1);
    ex = X(2);
    ey = Y(2);
    % Get line pixels coordinates
    
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    idxs = sub2ind(size(A), ly, lx);
    
    A(idxs) = A(idxs) + 1;
    % Increments accumulator
    %for j=1:length(lx)
    %    A(ly(j), lx(j)) =  A(ly(j), lx(j)) + 1;
    %end
    %Display
%      imagesc(A);
%      pause(0.05);
end
end