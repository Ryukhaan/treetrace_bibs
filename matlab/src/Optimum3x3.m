function [N, O] = Optimum3x3(I)
% Assume that I is a grayscale image

fh = [
    -0.112737, 0, 0.112737;
    -0.274526, 0, 0.274526;
    -0.112737, 0, 0.112737];
fv = fh';

Gh = double(imfilter(I, fh, 'replicate', 'same'));
Gv = double(imfilter(I, fv, 'replicate', 'same'));
N = sqrt(Gv .* Gv + Gh .* Gh);

O = atan2(Gv, Gh);
% J1 = 2 .* Gv .* Gh;
% J2 = Gv .* Gv - Gh .* Gh;
% 
% sJ1 = blockproc(J1, [8, 8], @(x) sum(x.data, 'all'));
% sJ2 = blockproc(J2, [8, 8], @(x) sum(x.data, 'all'));
% O = 0.5 .* atan2(sJ1, sJ2);
% O = 0.5 .* atan2(J1, J2);
% O = imresize(O, size(I), 'nearest');

end