function J = Norell(img)
% 
% Norell et al. 2011

if length(size(img)) == 3
    I = rgb2gray(img);
else
    I = img;
end
% Resize image
%I = imresize(I, [1080, 1440]);

% Parameters
sigma = 20;
sigmaf = 5;
sigmal = 20;

% Get middle and define sigma for gaussian
[h, w] = size(I);
xc = single(w / 2 + 1);
yc = single(h / 2 + 1);

% Create circle to occult boundaries
[X, Y] = meshgrid(1:w, 1:h);
myCircle = (X-xc).^2 + (Y-yc).^2 <= (xc - 3 * sigma).^2;
% Gaussian filter

myFilt = imgaussfilt(double(myCircle), sigma);
% Pixelwise with sub-image
ti = myFilt .* double(I);

% Compute FFT
F = fftshift(fft2(ti));
%Fa = log(abs(F));
Fa = log(abs(real(F)));
% Gaussian Filter
Fb = imgaussfilt(Fa, sigmaf);
Fc = Fb;
% Remove 15 percent maximum value (almost DC e.g.)
Fc(Fc > 0.85*max(Fc(:))) = 0.0;

% Calculations, orientation peaks
[r, c] = size(Fc);
angles = linspace(0, pi, 90);
angles(end) = [];

total = zeros(1, length(angles));
old_total = 0;
argi = 1;
for i=1:length(angles)
    % line equation
    % y = tan(theta)(x-a) + b
    f = @(x) tan(angles(i))*(x-xc) + yc;
    g = @(y) (y-yc)/tan(angles(i)) + xc;
    
    if angles(i) >= 0 && angles(i) <= pi/4
        sx = 1;
        sy = f(1);
    end
    if angles(i) > pi/4 && angles(i) <= 3*pi/4
        sx = g(1);
        sy = 1;
    end
    if angles(i) > 3*pi/4 && angles(i) <= 5*pi/4
        sx = c;
        sy = f(c);
    end
    
    ex = round(c/2);
    ey = round(r/2);
    [lx, ly] = bresenham(sx, sy, ex, ey);

    %idy = ( ly<1 | ly>r );        % indices of outside intersections
    %idx = ( lx<1 | lx>c );
    %ids = [idx; idy];
    vals = 0;
    k = 0;
    for j=1:length(lx)
        if lx(j)<1 || lx(j)>c || ly(j)<1 || ly(j)>r
            continue
        end
        vals = vals + Fc(ly(j), lx(j));
        k = k + 1;
    end
    %vals = diag(FI(ly(~idy), lx(~idx)));
    total(i) = vals ./ k;
    if total(i) > old_total
        old_total = total(i);
        %argi = i;
        lmx = lx;
        lmy = ly;
    end
    
    %figure(1), scatter(lx, ly, 2, 'b', 'filled')    % plot line
    %figure(2), plot(vals, 'Color', clr(i,:))    % plot profile
    
end

lineFilter = zeros(r, c);
for i=1:length(lmx)
    if lmx(i)<1 || lmx(i)>c || lmy(i)<1 || lmy(i)>r
        continue
    end
    lineFilter(lmy(i), lmx(i)) = 1;
end
lineFilter = imgaussfilt(lineFilter, sigmal);
lineFilter = 1 - lineFilter;
lineFilter = (lineFilter - min(lineFilter(:))) / (max(lineFilter(:)) - min(lineFilter(:)));


Ft = fftshift(fft2(I));
% GI = 0.2989 * I(:,:,1) + 0.5870 * I(:,:,2);
% Ft = fftshift(fft2(GI));
% lineFilter = imresize(lineFilter, [1620, 2160]);
res = Ft .* lineFilter;
J = real(ifft2(ifftshift(res)));

m = min(J(:));
M = max(J(:));
J = (J - m) / (M - m);
end



