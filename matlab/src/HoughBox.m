function [aabb] = boundingBox(I)
    % Returns
    % -------
    % aabb : pmin and pmax as (x,y)
    %
    [h, w] = size(I);
    I2 = imresize(I, [45, 60]);
    ratio = h/45;
    if length(size(I)) == 3
        I2 = rgb2gray(I2);
    end
    %I2 = medfilt2(I2, [9 9]);
    I2 = imbinarize(I2);
    % I2 = medfilt2(I2, [5 5]);
    % [E, ~, Gx, Gy] = edge(I2, 'Sobel');
    % G = sqrt(double(Gx .* Gx + Gy .* Gy));
    
    for i=6:10:40
        [centers, radii, ~] = imfindcircles(I2, [i 3*i-1], ...
            'ObjectPolarity', 'bright');
        if ~(isempty(centers))
            break
        end
    end
    if isempty(centers)
        aabb = [1, h, 1, w];
    else
        % 108
        pmin = ratio * (centers(1,:) - radii(1));
        pmin(pmin<1) = 1;
        if pmin(1) > w; pmin(1) = w; end
        if pmin(2) > h; pmin(2) = h; end
        
        pmax = ratio * (centers(1,:) + radii(1));
        pmax(pmax<1) = 1;
        if pmax(1) > w; pmax(1) = w; end
        if pmax(2) > h; pmax(2) = h; end

        aabb = [pmin(2), pmax(2), pmin(1), pmax(1)];
        
        % Display
        %imshow(I); hold on;
        %viscircles(ratio*centers(1,:), ratio*radii(1), 'Color', 'b');
        %pause
    end
end