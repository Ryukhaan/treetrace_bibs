function [E, N, O] = ConnerOptimum(I, ws)

% Optimum 3x3 by Ando 2000
fh = [
    -0.112737, 0, 0.112737;
    -0.274526, 0, 0.274526;
    -0.112737, 0, 0.112737];
fv = fh';

Gh = double(imfilter(I, fh, 'replicate', 'same'));
Gv = double(imfilter(I, fv, 'replicate', 'same'));

% Magnitude
N = sqrt(Gv .* Gv + Gh .* Gh);

% Orientation by Bo 2008
%O = atan2(Gv, Gh);
J1 = 2 .* Gv .* Gh;
J2 = Gv .* Gv - Gh .* Gh;
sJ1 = blockproc(J1, [ws, ws], @(x) sum(x.data, 'all'));
sJ2 = blockproc(J2, [ws, ws], @(x) sum(x.data, 'all'));
O = 0.5 .* atan2(sJ1, sJ2);

% Orientation Conner 1999
thresh = multithresh(O,7);
G = imquantize(O, thresh);
G = G+1;
G(G==9)=1;
G = mod(G-1, 4);
G = imresize(G, size(I), 'nearest');

O = imresize(O, size(I), 'nearest');

% NMS
E = NonMaximaSuppression(N, G);
E = (E-min(E(:))) / (max(E(:)) - min(E(:)));

[counts,~] = imhist(E);
%T = otsuthresh(counts);
T = triangle_th(counts, 255);
E = imbinarize(E, T);

%E = NMSG(N);
%E = ordfilt2(N,25,true(5));
%E = imhmax(N, 40);
% E = blockproc(N, [5, 5], @(x) NMSB(x.data));
end

function A = NonMaximaSuppression(N, G)
[h, w] = size(N);
A = zeros(size(N));
p=1; %padding
Npad=zeros(size(N)+2*p,class(N));
Npad(p+1:end-p,p+1:end-p)=N;
for row=1:h
    for col=1:w
        direction = G(row, col);
        cp = Npad(row+p, col+p);
        switch direction
            case 0
                p1 = Npad(row+p-1, col+p);
                p2 = Npad(row+p+1, col+p);
            case 1
                p1 = Npad(row+p-1, col+p+1);
                p2 = Npad(row+p+1, col+p-1);
            case 2
                p1 = Npad(row+p, col+p-1);
                p2 = Npad(row+p, col+p+1);
            case 3
                p1 = Npad(row+p-1, col+p-1);
                p2 = Npad(row+p+1, col+p+1);
        end
        mp = max([cp, p1, p2]);
        if mp == cp
            %if p1 > p2
                A(row, col) = mp;
            %end
        end
    end
end
end

function A = NMSG(N)
[h, w] = size(N);
A = zeros(size(N));
for i=4:h-4
    for j=4:w-4
        m = max(max(N(i-3:i+3, j-3:j+3)));
        if N(i, j) == m && m ~= 0
            A(i, j) = 1;
        end
    end
end
end

function A = NMSB(N)
A = zeros(size(N));
m = max(max(N));
if m ~= 0
    i = N==m;
    A(i) = 1;
end
end