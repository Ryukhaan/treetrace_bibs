function [N, O] = Conner(img, rois)
%
% Conner PhD 1999
%
I = img;
if length(size(I)) == 3
    I = rgb2gray(I);
end

% Gaussian Filter
w = fspecial('gaussian', 3, 1.0);
I = imfilter(I, w);
%I = medfilt2(I, [3, 3]);

% Sobel Operator
[~, ~, Gv, Gh] = edge(I, 'Sobel');
N = sqrt(Gv.^2 + Gh.^2);
% N = medfilt2(N, [3,3]);

% Arctan
O = atan2(Gv, Gh);

% FingerPrint
% J1 = 2 .* Gv .* Gh;
% J2 = Gv .* Gv - Gh .* Gh;
% sJ1 = blockproc(J1, [5, 5], @(x) sum(x.data, 'all'));
% sJ2 = blockproc(J2, [5, 5], @(x) sum(x.data, 'all'));
% O = 0.5 .* atan2(sJ1, sJ2);
% O = imresize(O, size(I), 'nearest');

% Quantize Direction into 8
thresh = multithresh(O,8);
O = imquantize(O, thresh);
O = O+1;
O(O==10)=1;

G = N;
G = blockproc(G, [rois rois], @(x) GlobalDirection(x.data, O));
G = G-1;
G = imresize(G, size(I), 'nearest');
O = G;

N = NonMaximaSuppression(N, G);
end

function D = GlobalDirection(X, Y)
counter = ones(1, 8);
gradmag = zeros(1, 8);
[h, w] = size(X);
n = h * w;
for i=1:n
    idx = Y(i);
    counter(idx) = counter(idx) + 1;
    gradmag(idx) = gradmag(idx) + X(i);
end
avg = gradmag ./ counter;
[~, no] = max(avg(:));
D = no .* ones(size(X));
end

function A = NonMaximaSuppression(N, G)
[h, w] = size(N);
A = zeros(size(N));
p=1; %padding
Npad=zeros(size(N)+2*p,class(N));
Npad(p+1:end-p,p+1:end-p)=N;
for row=1:h
    for col=1:w
        direction = G(row, col);
        direction = mod(direction-1, 4);
        cp = Npad(row+p, col+p);
        switch direction
            case 0
                p1 = Npad(row+p-1, col+p);
                p2 = Npad(row+p+1, col+p);
            case 1
                p1 = Npad(row+p-1, col+p+1);
                p2 = Npad(row+p+1, col+p-1);
            case 2
                p1 = Npad(row+p, col+p-1);
                p2 = Npad(row+p, col+p+1);
            case 3
                p1 = Npad(row+p-1, col+p-1);
                p2 = Npad(row+p+1, col+p+1);
            otherwise
                direction
        end
        mp = max([cp, p1, p2]);
        if mp == cp
            %if p1 > p2
                A(row, col) = 1;
            %end
        end
    end
end
end