J = imread('/Users/VoidMain/Desktop/Doctorat/Echantillonnage/Douglas/Log_ends_Besle_Lumix_20181004/16.jpg');
K = 0.2989 * J(:,:,1) + 0.5870 * J(:,:,2);

K = imresize(K, [1080, 1440]);
%B = boundingBox(K);
% Retrieve sub-image includes in the previous box
%K = K(B(1,2):B(2,2), B(1,1):B(2,1));
I = K;%imresize(K, [576, 768]);

blockSize = 36;
[h, w] = size(I);
wi = w / blockSize;
hi = h / blockSize;

n = hi*wi;
blocks = zeros(blockSize, blockSize, hi*wi);
count = 1;
[pX, pY] = meshgrid(1:wi, 1:hi);

fftfun = @(x) 1+log(abs(fftshift(fft2(x.data))));
B = blockproc(I, [blockSize, blockSize], fftfun);

%fftfilt = ;
B = blockproc(B, [blockSize, blockSize], @(x) x.data .* (x.data~=max(x.data(:))));
%Bf = blockproc(B, [blockSize, blockSize], @(x) x.data .* (x.data>=0.85*max(x.data(:))));
%Bb = blockproc(B, [blockSize, blockSize], @(x) (x.data>=0.85*max(x.data(:))));
figure(1);
imagesc(B);

tic;
C = blockproc(Bf, [blockSize, blockSize], @(x) computeAngle(x));
figure(2);
imagesc(C); colormap gray;
toc;

tic;
Ei = blockproc(Bb, [blockSize, blockSize], @(x) pcaAnalysis(x.data));
% T1 = 90;
% D = blockproc(I, [blockSize, blockSize], @(x) sobelNorm(x.data, T1));
figure(3);
imagesc(Ei); colormap gray;
toc;

% F = fleurModif(D, C);
% figure(4);
% imagesc(F);
myLines = zeros(hi*wi, 2);
for row=1:hi
    for col=1:wi
        theta = 180 * Ei(row, col) / pi;
        dx = (pX(row, col)-1) * blockSize + blockSize/2;
        dy = (pY(row, col)-1) * blockSize + blockSize/2;
        a = tan(theta);
        b = dy-tan(theta)*dx;
        myLines((row-1) * wi + col, :) = [a, b];
        
        x1 = 1;
        x2 = dx;
        y1 = a * x1 + b;
        y2 = dy;
        figure(1), line([x1, x2], [y1, y2], 'Color', 'r');
    end
end

acc = zeros(size(I));
n_lines = hi*wi;
if mod(n_lines, 2) == 1
    n_lines = n_lines - 1;
end
for i=1:2:n_lines
    for j=1:2:n_lines
        if i ~= j
            a = myLines(i, 1);
            b = myLines(i+1, 2);
            c = myLines(j, 1);
            d = myLines(j+1, 2);
            
            %p = (droites(i+2) + droites(j+2)) / 2;
            p = 1;
            if a == c
                continue
            end
            % (ix, iy) point where y=ax+b and y=cx+d intersects
            ix = (d - b) / (a - c);
            iy = (a * (d - b) / (a - c)) + b;
            % If (ix, iy) is on image
            if ix >= 1 && ix < w && iy >= 1 && iy < h
                %             ix = round(ix);
                %             iy = round(iy);
                % debug = ix;
                ix = floor(ix) + 1;
                iy = floor(iy) + 1;
                acc(iy, ix) =  acc(iy, ix) + p;
            end
        end
    end
end

figure(2);
acc = imgaussfilt(acc, 2.0);
imagesc(acc); hold on;

[r, c] = find(acc==max(acc(:)));
imshow(I);
hold on;
scatter(c, r, 64, 'r', 'filled');

function [ang] = computeAngle(X)
[r, c] = size(X.data);
xc = c/2;
yc = r/2;

angles = linspace(0, pi, 36);
angles(end) = [];
total = zeros(1, length(angles));
old_total = 0;
ang = 0;
%figure(1), imagesc(X.data), hold on;
for i=1:length(angles)
    % line equation
    % y = tan(theta)(x-a) + b
    f = @(x) tan(angles(i))*(x-xc) + yc;
    g = @(y) (y-yc)/tan(angles(i)) + xc;
    
    if angles(i) >= 0 && angles(i) <= pi/4
        sx = 1;
        sy = f(1);
    end
    if angles(i) > pi/4 && angles(i) <= 3*pi/4
        sx = g(1);
        sy = 1;
    end
    if angles(i) > 3*pi/4 && angles(i) <= 5*pi/4
        sx = c;
        sy = f(c);
    end
    if angles(i) > 5*pi/4 && angles(i) <= 7*pi/4
        sx = g(r);
        sy = r;
    end
    if angles(i) > 7*pi/4 && angles(i) <= 2*pi
        sx = 1;
        sy = f(1);
    end
    
    ex = round(c/2)+1;
    ey = round(r/2)+1;
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    %idy = ( ly<1 | ly>r );        % indices of outside intersections
    %idx = ( lx<1 | lx>c );
    %ids = [idx; idy];
    vals = 0;
    k = 0;
    for j=1:length(lx)
        if lx(j)<1 || lx(j)>c || ly(j)<1 || ly(j)>r
            continue
        end
        vals = vals + X.data(ly(j), lx(j));
        k = k + 1;
    end
    %vals = diag(FI(ly(~idy), lx(~idx)));
    total(i) = vals ./ k;
    if total(i) > old_total
        old_total = total(i);
        ang = angles(i);
    end
    
    %figure(1), scatter(lx, ly, 36, 'b', 'filled');
    %pause(0.05);
end
end

function [Fs] = computeFilter(x)
Fs = x;
% DC value
pk1 = max(Fs(:));
[r1, c1] = find(Fs==pk1);
Fs(r1, c1) = 0;
% Second peak
pk2 = max(Fs(:));
[r2, c2] = find(Fs==pk1);
lambda = 0.6
T = 2;
if pk2 / pk1 >= lambda
    T = 3;
end
end

function [ang] = pcaAnalysis(X)
[yj, xi] = find(X);
[coeff, score, latent, ~, explained, mu] = pca([xi, yj], 'Algorithm', 'eig');
[~, im] = max(explained);
ang = acos(dot(coeff(:,im), [1;0]));
end