function [acc, r, c] = Longuetaud(I, G, O)
% 
% Longuetaud et al. 2004
%

% We assume that Gradient, Orientation and Preprocessing have been done.
%
% Parameters
% ----------
% I : RGB or grayscale image.
% G : Gradient
% O : Orientation

J = I;
if length(size(I)) == 3
     J = rgb2gray(I);
end

[h, ~] = size(J);
scale = h / 405;
J = imresize(J, [405, 540]);

% Create accumalator
[h, w] = size(G);
acc = zeros(size(G));
[erows, ecols] = find(G);

%index = randperm(length(erows));
%mybar = waitbar(0, 'Accumulation on going...');
n = length(erows);
for i=1:n
    row = erows(i);
    col = ecols(i);
    
    theta = O(row, col);
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersect : y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersect : x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersect : y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    if a < 1e16
        % Intersect : x = w
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get only (X,Y) in image bounds
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    
    sx = X(1);
    sy = Y(1);
    ex = X(2);
    ey = Y(2);
    % Get line pixels coordinates
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    % Increments accumulator
    for j=1:length(lx)
        acc(ly(j), lx(j)) =  acc(ly(j), lx(j)) + 1;
    end
    
    %waitbar(double(i) / double(n), mybar, 'Accumulation on going...');
    % Display
    %imagesc(acc);
    %pause(0.05);
end
[r, c] = find(acc==max(acc(:)));
r = scale * r;
c = scale * c;
%disp('Pith Extract Done !');
toc;
end