%% input
point = [xc yc];               % pixel location
FI = Fc;

%% calculations
[r c] = size(FI);
angles = linspace(0, pi, 7);
angles(end) = [];
clr = lines( length(angles) );   % get some colors

figure(1), imagesc(FI), hold on
figure(2), hold on

total = zeros(1, length(angles));
for i=1:length(angles)
    % line equation
    % y = tan(theta)(x-a) + b
    f = @(x) tan(angles(i))*(x-point(1)) + point(2);
    g = @(y) (y-point(2)) / tan(angles(i)) + point(a);
    
    sx = 1;
    sy = f(1);
    if sy < 1
        sx = g(1);
        sy = 1;
    else
        if sy > r
            sx = g(r);
            sy = r;
        end
    end
    
    ex = c;
    ey = 
    % get intensities along line
    x = 1:c;
    y = round(f(x));
    idx = ( y<1 | y>r );        % indices of outside intersections
    vals = diag(FI(x(~idx), y(~idx)));
    
    total(i) = sum(vals(:));
    %clr(i) = sum(vals(:));
    figure(1), scatter(x, y, 16, clr(i,:), 'filled')    % plot line
    figure(2), plot(vals, 'Color', clr(i,:))    % plot profile
    %pause(0.1);
end

%plot(1:1:360, total);
hold off