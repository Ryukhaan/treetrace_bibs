function [acc] = fleurModif(E, O)
[h, w] = size(E);
acc = zeros(size(E));
[erows, ecols] = find(E);
for i=1:length(erows)
    row = erows(i);
    col = ecols(i);
    
    or = floor((erows(i) / 36) + 1);
    oc = floor((erows(i) / 36) + 1);
    if or>30; or=30; end;
    if oc>40; oc=40; end;
    theta = 180 .* O(or, oc) ./ pi;
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersect : y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersect : x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersect : y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    if a < 1e16
        % Intersect : x = w
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get only (X,Y) in image bounds
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    
    sx = X(1);
    sy = Y(1);
    ex = X(2);
    ey = Y(2);
    % Get line pixels coordinates
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    % Increments accumulator
    for j=1:length(lx)
        acc(ly(j), lx(j)) =  acc(ly(j), lx(j)) + 1;
    end
    
    % Display
    %imagesc(acc);
    %pause(0.05);
end
end