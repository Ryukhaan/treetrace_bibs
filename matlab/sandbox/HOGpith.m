% Read image
I = imread('/Users/VoidMain/Desktop/Doctorat/Echantillonnage/Douglas/Log_ends_Besle_Lumix_20181004/20.jpg');

I = imresize(I, [1080, 1440]);
% Convert image into grayscale
image = 0.5870 * I(:,:,1) + 0.1140 * I(:,:,2) + 0.2989 * I(:,:,3);

% Apply gaussian filter
image = imgaussfilt(image, 2.0);
T2 = 0.9;

% Parameters
% Window size
[h, w] = size(image);
% Cell size
cell_size = [12, 12];
% Number of cells in y-axis and x-axis
num_cells = [h / cell_size(1), w / cell_size(2)];
% Total of cells
n_subimages = num_cells(1) * num_cells(2);
% Num bins
bins = 9;
% Number of cells per bloc (x-axis and y-axis)
bx = 3;
by = 3;
tic;
% Extract HOGs (Kurdthongmee et al. 2018)
[features, vizu] = extractHOGFeatures(image, ...
    'CellSize', cell_size, ...
    'NumBins', bins, ...
    'BlockSize', [by, bx], ...
    'BlockOverlap', [0 0], ...
    'UseSignedOrientation', true);

% Display image
%figure;
% imshow(image);
% hold on;
% plot(vizu);

% Reshape HOGs like an n_subimages x bins matrix
hogs = zeros(bins, n_subimages);
%hogs = reshape(features, bins, n_subimages);
p = 1;
x = 1;
y = 1;
accy = 0;
accx = 0;
accb = 1;
for j=1:n_subimages
    i = (x-1) * num_cells(1) + y;
    hogs(:,i) = features(p:p+bins-1);
    p = p + bins;
    
    y = y + 1;
    accy = accy + 1;
    % Switch column cell-axis
    if mod(accy, by) == 0
        x = x + 1;
        y = y - by;
        accx = accx + 1;
        accy = 0;
    end
    % Switch bloc
    if mod(accx, bx) == 0 && mod(accy, by) == 0
        y = y + by;
        x = x - bx;
        accb = accb + 1;
        accx = 0;
    end
    % Switch column bloc-axis
    if mod(accb, num_cells(1)/by+1) == 0
        y = 1;
        x = x + bx;
        accb = 1;
    end
end
%hogs(:,1)

% All direction allowed
D = zeros(1, 4);
% Save [a, b] from line equation (y = ax + b)
droites = [];
% All angles available
angles = 360 - [110, 150, 190, 230, 270, 310, 350, 30, 70] + 90;
%angles = 360 - [75, 105, 135, 165, 195, 225, 255, 285, 315, 345, 15, 45];
%angles = 360 - [112.500000000000;157.500000000000;202.500000000000;247.500000000000;292.500000000000;337.500000000000;22.5000000000000;67.5000000000000]
% angles = 360 / bins .* (1:bins);

% Cell's middle (in pixels)
dx = cell_size(2) / 2;
dy = cell_size(1) / 2;
% Image's middle (in cells)
halfx = num_cells(2) / 2;
halfy = num_cells(1) / 2;

X = [];
Y = [];
for i=1:n_subimages
    [Fval, Farg] = max(hogs(:, i));
    %if Fval / sum(hogs(:, i)) < T2
    %    continue
    %end
    x = floor(i / num_cells(1)) + 1;
    y = mod(i-1, num_cells(1)) + 1;
    % Top left
    if x < halfx && y < halfy
        %D = [4,5,6,10,11,12];
        D = [8, 9, 3, 4];
    end
    % Top right
    if x >= halfx && y < halfy
        %D = [1,2,3,7,8,9];
        D = [2, 3, 6, 7];
    end
    % Bottom left
    if x < halfx && y >= halfy
        %D = [1,2,3,7,8,9];
        D = [1, 2, 6, 7];
    end
    % Bottom right
    if x >= halfx && y >= halfy
        %D = [4,5,6,10,11,12];
        D = [8, 9, 3, 4];
    end
    
    %For all line with same F
    all_w = sum(exp(hogs(:, i)));
    for k=1:bins
        if (hogs(k, i) == Fval) &&  (ismember(k, D))
            % Retrieve angle and take the perpendicular to that one
            p = exp(hogs(k, i)) / all_w;
            angle = angles(k);
            % Equation line : y = ax + b
            % Point-slope view given a point (a,b) and theta : y = tan(theta) (x-a) + b
            % Applying in our case : y = tan(theta) (x-dx) + dy
            % y = tan(theta).x + dy - tan(theta).dx
            a = tan( angle * pi / 180 );
            b = dy - a * dx;
            % Save (a, b)
            droites = [droites, [a, b, p]];
%             X = [X, dx];
%             Y = [Y, dy];
        end
    end
    % Increment cell's center
    dy = dy + cell_size(1);
    if mod(i, num_cells(1)) == 0
        dx = dx + cell_size(2);
        dy = cell_size(1) / 2;
    end
end

% Estimate all intersections
n_lines = size(droites);
quant = 10;
qx = w/quant;
qy = h/quant;
Acc = zeros(qy, qx);
%Acc = zeros(h, w);
for i=1:3:n_lines(2)
    for j=1:3:n_lines(2)
        if i ~= j
            a = droites(i);
            b = droites(i+1);
            c = droites(j);
            d = droites(j+1);
            
            %p = (droites(i+2) + droites(j+2)) / 2;
            p = 1;
            if a == c
                continue
            end
            % (ix, iy) point where y=ax+b and y=cx+d intersects
            ix = (d - b) / (a - c);
            iy = (a * (d - b) / (a - c)) + b;
            % If (ix, iy) is on image
            if ix >= 1 && ix < w && iy >= 1 && iy < h
                %             ix = round(ix);
                %             iy = round(iy);
                % debug = ix;
                ix = floor(ix / quant) + 1;
                iy = floor(iy / quant) + 1;
                Acc(iy, ix) =  Acc(iy, ix) + p;
            end
        end
    end
end
% for j=1:3:n_lines(2)
%     iy = 1:h;
%     a = droites(j);
%     b = droites(j+1);
%     %p = droites(j+2);
%     p = 1;
%     ix = round((iy - b) / a);
%     negx = find(ix<1); 
%     iy(negx(:)) = [];
%     ix(negx(:)) = [];
%     posx = find(ix>w);
%     iy(posx(:)) = [];
%     ix(posx(:)) = [];
%     %indexes = num2cell([iy; ix]);
%     %Acc(indexes{:}) = Acc(indexes{:}) + 1;
%     indexes = iy + (ix-1) * h;
%     Acc(indexes(:)) = Acc(indexes(:)) + p;
% end
%Acc = medfilt2(Acc, [3 3]);
[rows, cols] = find(Acc==max(Acc(:)));
%scatter(cols * quant, rows * quant, 64, 'y', 'filled');
toc;
subplot(1,2,1);
imshow(Acc, [min(Acc(:)) max(Acc(:))]);

subplot(1,2,2);
imshow(I);
hold on;
%plot(vizu);
%hold on;
xm = mean(cols);
ym = mean(rows);
scatter(xm * quant, ym * quant, 48, 'r', 'filled');