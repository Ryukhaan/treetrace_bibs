% input
point = [xc yc];               % pixel location
FI = Fc;

% calculations
[r c] = size(FI);
angles = linspace(0, pi, 180);
angles(end) = [];
clr = lines( length(angles) );   % get some colors
figure(1), imagesc(FI), hold on
%figure(2), hold on

total = zeros(1, length(angles));
tic;
for i=1:length(angles)
    % line equation
    % y = tan(theta)(x-a) + b
    f = @(x) tan(angles(i))*(x-point(1)) + point(2);
    g = @(y) (y-point(2))/tan(angles(i)) + point(1);
    
    if angles(i) >= 0 && angles(i) <= pi/4
        sx = 1;
        sy = f(1);
    end
    if angles(i) > pi/4 && angles(i) <= 3*pi/4
        sx = g(1);
        sy = 1;
    end
    if angles(i) > 3*pi/4 && angles(i) <= 5*pi/4
        sx = c;
        sy = f(c);
    end
    
    ex = round(c/2);
    ey = round(r/2);
    [lx, ly] = bresenham(sx, sy, ex, ey);

    idy = ( ly<1 | ly>r );        % indices of outside intersections
    idx = ( lx<1 | lx>c );
    %ids = [idx; idy];
    vals = 0;
    k = 0;
    for j=1:length(lx)
        if lx(j)<1 || lx(j)>c || ly(j)<1 || ly(j)>r
            continue
        end
        vals = vals + FI(ly(j), lx(j));
        k = k + 1;
    end
    %vals = diag(FI(ly(~idy), lx(~idx)));
    total(i) = vals ./ k;
    %total(i) = sum(vals(:)) ./ length(vals);
    %clr(i) = sum(vals(:));
    %figure(1), scatter(lx, ly, 2, 'b', 'filled')    % plot line
    %figure(2), plot(vals, 'Color', clr(i,:))    % plot profile
    %pause(0.1);
end
toc;

%total = medfilt1(total, 7);
%figure(2);
%plot(round(180 * angles ./ pi), total);
[val, arg] = max(total(:));

angles(arg)
aT = angles(arg);
f = @(x) tan(aT)*(x-point(1)) + point(2);
g = @(y) (y-point(2))/tan(aT) + point(1);
%aT = pi ./ 2;
lineFilter = zeros(r, c);
if aT >= 0 && aT <= pi/4
    sx = 1;
    sy = f(1);
end
if aT > pi/4 && aT <= 3*pi/4
    sx = g(1);
    sy = 1;
end
if aT > 3*pi/4 && aT <= 5*pi/4
    sx = c;
    sy = f(c);
end
ex = round(c/2);
ey = round(r/2);
[lx, ly] = bresenham(sx, sy, ex, ey);
idy = ( ly<1 | ly>r );        % indices of outside intersections
idx = ( lx<1 | lx>c );
for i=1:length(lx)
    if lx(i)<1 || lx(i)>c || ly(i)<1 || ly(i)>r
        continue
    end
    lineFilter(ly(i), lx(i)) = 1;
end
lineFilter = imgaussfilt(lineFilter, 50.0);
lineFilter = (lineFilter - min(lineFilter(:))) / (max(lineFilter(:)) - min(lineFilter(:)));
lineFilter = 1 - lineFilter;

Ft = fftshift(fft2(si));
res = Ft .* lineFilter;
Ires = abs(ifft2(fftshift(res)));

figure(1);
subplot(1,2,1);
%imagesc(1 + log(abs(res)));
imshow(Ires, [min(Ires(:)), max(Ires(:))]);
%imshow(lineFilter);

subplot(1,2,2);
imshow(si, [min(si(:)), max(si(:))]);

hold off