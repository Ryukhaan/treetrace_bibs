%I = imread('/Users/VoidMain/Desktop/Doctorat/Echantillonnage/Douglas/preprocessed48m.jpg');
I = imread('/Users/VoidMain/Desktop/Doctorat/Echantillonnage/Douglas/Log_ends_Besle_Lumix_20181004/16.jpg');
%function [r, c] = Fleur(I)
tic;
I = 0.2989 * I(:,:,1) + 0.5870 * I(:,:,2);
%I = rgb2gray(I);

I = imresize(I, [648, 864]);
%I = preprocessing(I);

scale = 0.5;
J = imresize(I, scale);

% Compute Sobel Filter
hx = [1 2 1]';
hy = [-1 0 1];
fx = hx * hy;
fy = fx';

Gx = double(imfilter(J, fx));
Gy = double(imfilter(J, fy));

% Get magnitude and orientation
N = sqrt(Gx.^2 + Gy.^2);
m = min(N(:));
M = max(N(:));
N = 255 .* ((N - m)  / (M - m));
O = atan2(Gy, Gx);

% Threshhold magnitude
T1 = 60;
N(N < T1) = 0;
N(N >= T1) = 1;
% Create accumalator
[h, w] = size(N);
acc = zeros(size(N));
[erows, ecols] = find(N);

%index = randperm(length(erows));

for i=1:length(erows)
    row = erows(i);
    col = ecols(i);
    
    theta = O(row, col);
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersect : y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersect : x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersect : y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    if a < 1e16
        % Intersect : x = w
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get only (X,Y) in image bounds
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    
    sx = X(1);
    sy = Y(1);
    ex = X(2);
    ey = Y(2);
    % Get line pixels coordinates
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    % Increments accumulator
    for j=1:length(lx)
        acc(ly(j), lx(j)) =  acc(ly(j), lx(j)) + 1;
    end
    
    % Display
    %imagesc(acc);
    %pause(0.05);
end
toc;
figure(1);
imagesc(acc);

figure(2);
[r, c] = find(acc==max(acc(:)));
imagesc(I);
hold on;
scatter((1/scale)*c, (1/scale)*r, 64, 'r', 'filled');
%end