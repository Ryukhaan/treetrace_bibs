function [C, O] = laplacianPyramid(I, sigma, smooth, beta, bw, Ts, si)
%J = I;
%J = imresize(J, 0.5);
% hsv = rgb2hsv(J);
% J = rgb2gray(J);
% 
% [h, w] = size(J);
% h = h / B; w = w / B;
%m = mean(hsv(:,:,1), 'all');
% m = hsv(size(hsv,1)/2, size(hsv,2)/2, 1);
% X = hsv(:,:,1);
% X(X>1.4*m)=0;
% X(X<0.6*m)=0;
% X = uint8(X~=0) .* J;
% P = imgaussfilt(X, sigmal) - imgaussfilt(X, sigmah);
% [~, O] = imgradient(P);
% E = adapthisteq(P, 'NumTiles', [h, w], 'ClipLimit', 0.05);
% 
% [H, ~] = imhist(E, 256);
% level = otsuthresh(H);
% T = imbinarize(E, level);
% S = bwskel(T);
P = cell(1,3);
C = cell(1,3);
O = cell(1,3);

X = I;
for i=1:2
    F = imgaussfilt(X, sigma);
    G = imgaussfilt(X, sigma+1);
    P{1,i} = double(G - F);
    X = imresize(X, 0.5);
end

ws = 9;
m = ceil(ws/2);
[X, Y] = meshgrid(1:ws, 1:ws);
%hx = exp(-beta * (Y-m).^2) .* ( X .* exp(-beta * (X-m).^2));
%hy = exp(-beta * (X-m).^2) .* ( Y .* exp(-beta * (Y-m).^2));
h = complex(X,Y) .* exp(-beta * (X.^2 + Y.^2));
hw = complex(fspecial('gaussian',[17, 17], smooth));
for i=1:2
    gradf = imfilter(P{i}, h);
    %M = abs(gradf).^2;
    S = gradf .^ 2;
    %E = M .* exp(2*angle(gradf));
    T = imfilter(S, hw); %./ imfilter(abs(S), hw);
    C{1, i} = abs(T);
    O{1, i} = angle(T);
end

[h, w] = size(I);
[H, W] = size(I);
A = zeros(size(I));
Oi = O{1};
Ci = C{1};
for i=1:bw:h
    for j=1:bw:w
        ww = Ci(i:i+bw-1, j:j+bw-1);
        [m, idx] = max(ww(:));
        if m < Ts
            continue
        end
        theta = Oi(idx);
        xc = j + ceil(bw/2);
        yc = i + ceil(bw/2);
        
        a = tan(theta);
        f = @(x) a*(x-xc) + yc;
        g = @(y) (y-yc)/a + xc;
        
        %b = row - tan(theta) * col;
        
        X = [0, 0, 0, 0];
        Y = [0, 0, 0, 0];
        
        % Intersect : y = 1
        if a ~= 0
            X(1) = g(1);
            Y(1) = 1;
        end
        % Intersect : x = 1
        if a < 1e16
            X(2) = 1;
            Y(2) = f(1);
        end
        % Intersect : y = h
        if a ~= 0
            X(3) = g(H);
            Y(3) = H;
        end
        if a < 1e16
            % Intersect : x = w
            X(4) = W;
            Y(4) = f(W);
        end
        
        % Get only (X,Y) in image bounds
        PX = (X >= 1 & X <= W);
        PY = (Y >= 1 & Y <= H);
        X = X(PX & PY);
        Y = Y(PX & PY);
        
        if length(X) < 2 ||length(Y) < 2
            continue
        end
        
        sx = X(1);
        sy = Y(1);
        ex = X(2);
        ey = Y(2);
        % Get line pixels coordinates
        
        [lx, ly] = bresenham(sx, sy, ex, ey);
        
        idxs = sub2ind(size(A), ly, lx);
        
        A(idxs) = A(idxs) + 1;
        %imagesc(A), hold on;
        %scatter(xc, yc, 32, 'r', 'filled');
        %pause(0.01);
    end
end
A = imgaussfilt(A, 2.0);
m = max(A(:));
A = A(A>0.9*m);
X = find(A>0);
idx = mean(X, 'all');
[y, x] = ind2sub(size(A), idx);
imagesc(A), hold on, scatter(x, y, 32, 'r', 'filled');
end