function A = BigunM(img, npyms, sigmap, beta, w, Ts, sigmao)
%Ep = cell(1, npyms);
Lp = cell(1, npyms);
gradf = cell(1,npyms);
z = cell(1,npyms);
I = double(img);
[X, Y] = meshgrid(1:w, 1:w);
X = X - ceil(w/2);
Y = Y - ceil(w/2);
hx = exp(-beta * Y.^2) .* (X .* exp(-beta * X.^2));
hy = exp(-beta * X.^2) .* (Y .* exp(-beta * Y.^2));
hg = fspecial('gaussian', [17, 17], 1.0);

X = I;
for i=1:npyms
    X = I;
    F = imgaussfilt(I, sigmap);
    G = imgaussfilt(I, 2*sigmap);
    Lp{i} = F - G;
    I = imresize(Lp{i}, 0.5);
    %Ep{i} = imgaussfilt(Lp{i}.^2, 4);
    gradf{i} = complex(imfilter(Lp{i}, hx), imfilter(Lp{i}, hy));
    squared =  gradf{i} .^ 2;
    z{i} = imfilter(squared, hg) ./ imfilter(abs(squared), hg);
end

idx = 2;
[h, w] = size(Lp{idx});
%[H, W] = size(Lp{idx});
A = zeros(size(Lp{idx}));
Oi = angle(z{idx});
Oi = imgaussfilt(Oi, sigmao(1));
%Ci = Lp{idx};

G = Lp{idx};
G = uint8(256 .* (G-min(G(:))) / (max(G(:)) - min(G(:))));
%imwrite(G, '../results/gradient.jpg');
G(G~=86)=0;
G(G~=0)=1;
% level = graythresh(G);
% G = imbinarize(G, level);
% G = bwskel(G);
%G(G>0.5+epsilon(1)) = 0;
%  G(G>=0) = 0;
%  G(G<-1) = 0;
%  G(G~=0) = 1;
[erows, ecols] = find(G);
n = length(erows);

% E = find(G);
% if length(E) < 5000
%     [erows, ecols] = find(G);
%     n = length(erows);
% else
%     idxs = E(randi(numel(E), 5000, 1));
%     [erows, ecols] = ind2sub(size(G), idxs);
%     n = length(erows);
% end
for i=1:n
    row = erows(i);
    col = ecols(i);
    
    theta = Oi(row, col);
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersect : y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersect : x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersect : y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    if a < 1e16
        % Intersect : x = w
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get only (X,Y) in image bounds
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    
    if length(X) < 2 ||length(Y) < 2
        continue
    end
        
    sx = X(1);
    sy = Y(1);
    ex = X(2);
    ey = Y(2);
    % Get line pixels coordinates
    
    [lx, ly] = bresenham(sx, sy, ex, ey);
    
    idxs = sub2ind(size(A), ly, lx);
    
    A(idxs) = A(idxs) + 1;
    % Increments accumulator
    %for j=1:length(lx)
    %    A(ly(j), lx(j)) =  A(ly(j), lx(j)) + 1;
    %end
    %Display
%      imagesc(A);
%      pause(0.05);
end
%A = imgaussfilt(A, 1);
%s1 = std(A, [], 'all');

% Take only Ts * max
%m  = max(A(:));
% idx = find(A>Ts*m);
% %idm = mean(idx(:));

% Take max
% [~, idx] = max(A(:));
% 
% [y, x] = ind2sub(size(A), idx);
% ym = mean(y, 'all');
% xm = mean(x, 'all');

% Take barycenter weighted (idx > ts*m)
m  = max(A(:));
idx = find(A>Ts(1)*m);
[y, x] = ind2sub(size(A), idx);
ym = sum(y .* A(idx), 'all') ./ sum(A(idx), 'all');
xm = sum(x .* A(idx), 'all') ./ sum(A(idx), 'all');

%imagesc(log(1+A)), hold on, scatter(xm,ym,32,'r','filled');
% clf;
% subplot(1,2,2), imagesc(Lp{2}), colormap gray, hold on, scatter(xm,ym,32,'r','filled');
% subplot(1,2,1), imagesc(A);


idx = 1;
[H, W] = size(Lp{idx});
Oi = angle(z{idx});
%Oi = imgaussfilt(Oi, 1.0);
%Ci = abs(theta{idx});

%b=32;
sy = ceil(max(1, 2*ym-512));
ey = ceil(min(H, 2*ym+512));
sx = ceil(max(1, 2*xm-512));
ex = ceil(min(W, 2*xm+512));

G = Lp{idx};
G = G(sy:ey, sx:ex);
Oi = Oi(sy:ey, sx:ex);
Oi = imgaussfilt(Oi, sigmao(2));
A = zeros(size(G));
[h, w] = size(G);

G = uint8( 255 .* (G-min(G(:))) / (max(G(:)) - min(G(:))));
G(G>90) = 0;
G(G<82) = 0;
G(G~=0) = 1;
[erows, ecols] = find(G);
n = length(erows);
% E = find(G);
% if length(E) < 5000
%     [erows, ecols] = find(G);
%     n = length(erows);
% else
%     idxs = E(randi(numel(E), 5000, 1));
%     [erows, ecols] = ind2sub(size(G), idxs);
%     n = length(erows);
% end
for i=1:n
    row = erows(i);
    col = ecols(i);
    
    theta = Oi(row, col);
    f = @(x) tan(theta)*(x-col) + row;
    g = @(y) (y-row)/tan(theta) + col;
    % Line : y = ax + b
    a = tan(theta);
    %b = row - tan(theta) * col;
    
    X = [0, 0, 0, 0];
    Y = [0, 0, 0, 0];
    
    % Intersect : y = 1
    if a ~= 0
        X(1) = g(1);
        Y(1) = 1;
    end
    % Intersect : x = 1
    if a < 1e16
        X(2) = 1;
        Y(2) = f(1);
    end
    % Intersect : y = h
    if a ~= 0
        X(3) = g(h);
        Y(3) = h;
    end
    if a < 1e16
        % Intersect : x = w
        X(4) = w;
        Y(4) = f(w);
    end
    
    % Get only (X,Y) in image bounds
    PX = (X >= 1 & X <= w);
    PY = (Y >= 1 & Y <= h);
    X = X(PX & PY);
    Y = Y(PX & PY);
    
    if length(X) < 2 ||length(Y) < 2
        continue
    end
        
    start_x = X(1);
    start_y = Y(1);
    end_x = X(2);
    end_y = Y(2);
    % Get line pixels coordinates
    
    [lx, ly] = bresenham(start_x, start_y, end_x, end_y);
    
    idxs = sub2ind(size(A), ly, lx);
    
    A(idxs) = A(idxs) + 1;
    % Increments accumulator
    %for j=1:length(lx)
    %    A(ly(j), lx(j)) =  A(ly(j), lx(j)) + 1;
    %end
    %Display
%      imagesc(A);
%      pause(0.05);
end
%A = imgaussfilt(A, 2);

m  = max(A(:));
idx = find(A>Ts(2)*m);
[y, x] = ind2sub(size(A), idx);
ym = sum(y .* A(idx), 'all') ./ sum(A(idx), 'all');
xm = sum(x .* A(idx), 'all') ./ sum(A(idx), 'all');

%imagesc(log(1+A)), hold on, scatter(xm,ym,32,'r','filled');
clf;
ym = ym + sy;
xm = xm + sx;
imagesc(img), hold on,  colormap gray, scatter(xm,ym,32,'r','filled');
% subplot(1,2,2), imagesc(G), colormap gray, hold on, scatter(xm,ym,32,'r','filled');
% subplot(1,2,1), imagesc(A);

end