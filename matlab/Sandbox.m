% files = dir('/Users/VoidMain/Desktop/Doctorat/Echantillonnage/Douglas/Log_ends_Besle_Lumix_20181004_renommes/*.jpg');
% %myFile = zeros(length(files)+1, 3);
% myFile = [];
% myFile = [myFile; ["Name", "X", "Y"]];
% %myFile(1, :) = ["Name", "X", "Y"];
% i = 2;
% for file=files'
%     image = imread([file.folder, '/', file.name]);
%     image = sawMarksRemoval(image);
%     %[acc, r, c] = pithExtract(image, 70);
%     [~, ~, r, c] = HOGs(image, 12, 3, 10);
%     mr = mean(r(:));
%     mc = mean(c(:));
%     %myFile(i, :) = [string(file.name), num2str(mc), num2str(mr)];
%     myFile = [myFile; [string(file.name), num2str(mc), num2str(mr)]];
%     i = i + 1;
% end
% fid = fopen('pith_location_Douglas_Besle_hog.csv','wt');
% if fid>0
%     fprintf(fid,'%s,%s,%s\n',myFile(1,:));
%     for k=2:size(myFile,1)
%         fprintf(fid,'%s,%f,%f\n',myFile(k,:));
%     end
%     fclose(fid);
% end
 
%  % Create dataset
%[~, ~, positions] = xlsread('../../pith_location_spruce.xlsx');
% name = fileread('../../only_pith_name.csv');
% names = strsplit(name, '\n');
% nimages = length(names);
% [num_data ~] = xlsread('../../pith_location_spruce.xlsx');
% %[nimages, ~] = size(positions);
% for i=2:nimages
%     %tmp = positions(i, 1:5);
%     %if isnan(tmp{1})
%     %    tmp{2}
%     %end
%     %i
%     if num_data(i-1, 4) == 3240
%         scale = 6;
%     else
%         scale = 7.348148;
%     end
%     %      X = zeros(1, 360);
%     %      Y = zeros(1, 270);
%     %
%     %      Z = zeros(1, 360*270);
%     [X, Y] = meshgrid(1:720, 1:540);
%     U = zeros(540, 720);
%     yc = floor(num_data(i-1, 2)/scale)+1;
%     xc = floor(num_data(i-1, 1)/scale)+1;
%     U(yc, xc) = 1;
%     %U(yc, xc) = 1;
%     %      X(xc) = 1;
%     %      Y(yc) = 1;
%     %      cx = abs((X-xc))<= (3).^2;
%     %      cy = abs((Y-yc))<= (3).^2;
% %     cu = (X - xc) .^2 + (Y-yc).^2 <= (15).^2;
% %     fu = imgaussfilt(double(cu), 5);
% %     U = fu .* U;
% %     U = (U - min(U(:))) / (max(U(:)) - min(U(:)));
%     %      fx = imgaussfilt(double(cx), 2);
%     %      fy = imgaussfilt(double(cy), 2);
%     % Pixelwise with sub-image
%     
%     %output = imgaussfilt(output, 10);
%     %figure(1), imagesc(outputx);
%     %pause;
%     %folder = tempdir;
%     %A = [xc];
%     %Z(xc + (yc-1)*360) = 1;
%     %X(xc) = 1;
%     imwrite(U, fullfile('/Users/VoidMain/Desktop/Doctorat/cnnenv/dataset/target/', names{i}));
%     %dlmwrite(fullfile('./datas/NeuralNetwork/', tmp{1}), X);
% end

files = dir('../../Echantillonnage/Douglas/Log_ends_Besle_Lumix_20181004_renommes/*.jpg');
for file=files'
    img = imread([file.folder, '/', file.name]);
    %img = imresize(img, 1);
    %tic;
%     hsv = rgb2hsv(img);
      tic;
      img = rgb2gray(img);
      
      M = blockproc(img, [124, 124], @(x) mean(x.data, 'all'));
      S = blockproc(img, [124, 124], @(x) std(double(x.data), 0, 'all'));
      C = S ./ M;
      C(M==0) = 1e8;
      C = log(1+C);
      
      %C = imgaussfilt(C, 1);
      C = medfilt2(C, [3, 3]);
        
      %imwrite(C, ['../results/Contrast_mean/contrast_', file.name]);
      
      cm = C(ceil(size(C,1)/2), ceil(size(C,2)/2));
      
      BW = C;
      BW = (BW - min(BW(:))) / (max(BW(:)) - min(BW(:)));
      BW(abs(BW-cm) < 0.12) = 0;
      BW(BW > 0) = 1;
      BW = logical(BW);
      %imagesc(BW);
      
      %level = graythresh(C);
      %BW = imbinarize(C, level);
      %BW = edge(C, 'Canny');
      %BW = imbinarize(C);
      
      D = bwdist(~BW);
      D = -D;
      D(BW) = Inf;
      L = watershed(D);
      L(BW) = 0;
      
      mask = regionprops(L, 'Area');
      mask = cat(1, mask.Area);
      [~, idx] = max(mask(:));
      mask = imresize(L==idx, size(img), 'bicubic');
      section = img .* uint8(mask);
      
      %imwrite(section, ['../results/Watershed/median/watershed_', file.name]);
            
      %imagesc(section), colormap gray;
      %section = img(mask(2):mask(2)+mask(4), mask(1):mask(1)+mask(3));
      %imagesc(section);
      %imagesc(labeloverlay(img, imresize(L, size(img), 'nearest')));
%       [~, ~, Gv, Gh] = edge(section, 'Sobel');
%       G = double(Gv.^2 + Gh.^2);
%       O = atan2(Gh, Gv);
%       %T = mean(G, 'all');
%       G(G>0.01) = 0;
%       G = 255 * normalize(G, 'range');
%       level = graythresh(G);
%       G = imbinarize(G, level);
      %G(G<T) = 0;
      %G(G>=T) = 1;
%       acc = Longuetaud(G, O);
%       
%       %acc = Bigun(section, 2, 1.0, 3.0, 3, 20, 0.8, 2.0, 0.8);
%       section = Norell(section);
%       
       acc = BigunM(section, 2, 1.5, 3.0, 9, [0.7, 0.7], [5.0, 5.0]);
%       toc;

      %imagesc(log(1+acc));
%     hsv(:,:,2) = 0;
%     hsv(:,:,3) = 0;
%     m = mean(hsv(:,:,1), 'all');
%     h = hsv(:,:,1);
%     h(h>=m) = 0;
%     hsv(:,:,1) = h;
%     imgt = hsv(:,:,1);
%     imgt = uint8(255 * (imgt - min(imgt(:))) / (max(imgt(:)) - min(imgt(:))));
      %[~, O] = fftConfidence(img, 62);
      %R = Jain(imgt, 124);
      %img = adapthisteq(img, 'NumTiles', [2976/124, 3968/124], 'ClipLimit', 0.05);
      %[C, O] = laplacianPyramid(img, 2.5, 1.5, 1.5, 31, 0.2);
      
%       R = imgaussfilt(img, 5.0) - imgaussfilt(img, 4.0);
%       [h, w] = size(R);
%       W = zeros(size(R));
%       for i=1:h-2
%           for j=1:w-2
%               ww = R(i:i+2, j:j+2);
%               if R(i,j) >= max(ww(:))
%                   W(i,j) = 1;
%               end
%           end
%       end
      %E = adapthisteq(R, 'NumTiles', [2976/124, 3968/124], 'ClipLimit', 0.05);
      
      %level = graythresh(R);
      %E = imbinarize(R, level);
      
      %[A, M, O] = fftConfidence(E, 124, 23);
      %imgc = img .* uint8(R);
      %imagesc(R);
      %toc;
%     [~, ~, dx, dy] = edge(img, 'Sobel');
%     dx2 = dx.^2 - dy.^2;
%     dy2 = 2 * dx .* dy;
%     
%     
%     [nr,nc]=size(img);
%     [x y] = meshgrid(1:nc,1:nr);
%     u = dx2;
%     v = dy2;
%     quiver(x,y,u,v);
    %img = adapthisteq(img, 'NumTiles', [62, 62]);
    %[N, O, C, ~, ~, F] = Bo(img, 108, 0.01);
    %[B, O, C] = fftConfidence(img, 62);
    %[M,S,C,U,L] = Jain(img, 62);
    %montage({img,M,S,C,U,L});
    %imwrite(img .* uint8(C), ['../results/Watershed_Contrast/', file.name]);
    %subplot(1,3,1);
    %imagesc(B); colormap gray;
    %subplot(1,3,2);
    %imagesc(O);
    %subplot(1,3,3);
    %imagesc(C);
%     %N = Entacher(img, 5);
%     S = imresize(img, [186, 248]);
%     E = edge(S, 'Sobel');
%     D = bwdist(E);
%     D = -D;
%     D(E) = Inf;
%     L = watershed(D);
%     L(E) = 0;
%     
%     R = labeloverlay(S, L);
%     imshow(R);
%     label = L(size(L,1)/2, size(L,2)/2);
%     
%     L(L~=label) = 0;
%     L(L==label) = 1;
%     [row, col] = find(L);
%     aabb = 4 * [min(row), max(row), min(col), max(col)];
%     
%     S = imresize(img, [432, 578]);
%     S = S(aabb(1):aabb(2), aabb(3):aabb(4));
%     E = edge(S, 'Sobel');
%     D = bwdist(E);
%     D = -D;
%     D(E) = Inf;
%     L = watershed(D);
%     L(E) = 0;
    
    %R = labeloverlay(imresize(img, [432, 578]), L);
%    imagesc(L);
    %img = (img - min(img(:))) / (max(img(:)) - min(img(:)));
    %img = blockproc(img, [124, 124], @(x) Moments(x.data));
    %imagesc(img);
%     [S, D] = Moments(img, 1, 1);
%     E5 = [-1, -2, 0, 2, 1];
%     L5 = [1,4,6,4,1];
%     R5 = [1,-4,6,-4,1];
%     E5L5 = E5' * L5;
%     E5R5 = E5' * R5;
%     E5E5 = E5' * E5;
%     L5E5 = L5' * E5;
%     L5R5 = L5' * R5;
%     L5L5 = L5' * L5;
%     R5E5 = R5' * E5;
%     R5L5 = R5' * L5;
%     R5R5 = R5' * R5;
% %     
%     %fI = blockproc(double(img), [124,124], @(x) Moments(x.data));
%     %imagesc(fI); colormap gray;
%     F = imfilter(double(img), E5L5);
%     F = (F - min(F(:))) / (max(F(:)) - min(F(:)));
%     [N, O] = Sobel(F);
    %E = sum(fI, 'all')
    
    
    %idx = linspace(1,512,512);
    %T = 0.5 * sum(S .* idx, 'all');
    %T = sum(D .* (idx .^ 2), 'all');
    %T = sum(D .* (1 ./ (1 + idx.^2)), 'all');
%     T = sum(D.^2, 'all') .* sum(S.^2, 'all');
%     I = img;
%     I(I<T) = 0;
%     I(I>=T) = 1;
%     for i=[13,23,59,83,101,127]
%        [M, SD, S, K] = Moments(img, i)
%        imwrite(M, ['../results/Mean/', file.name, '_', num2str(i)]);
%        imwrite(SD, ['../results/Standard/', file.name, '_', num2str(i)]);
%        imwrite(S, ['../results/Skewness/', file.name, '_', num2str(i)]);%        imwrite(K, ['../results/Kurtosis/', file.name, '_', num2str(i)]);
%     end
    %pause(0.1);
end

% I = imread('/Users/VoidMain/Desktop/Doctorat/cnnenv/dataset/test/1.jpg');
% figure(1), imshow(I), hold on;
% % 
% x = 139;
% y = 299;
% figure(1), scatter(x, y, 32, 'r', 'filled');

% [~, ~, positions] = xlsread('./results/pith_location/�pic�a/pith_location_spruce.xlsx');
% [nimages, ~] = size(positions);
% for i=2:nimages
%     name = positions(i, 1);
%     img = imread(['/Users/VoidMain/Desktop/Doctorat/cnnenv/dataset/original/', name{1}]);
%     img = imresize(img, [405, 540]);
%     imwrite(img, ['/Users/VoidMain/Desktop/Doctorat/cnnenv/dataset/training/', name{1}]);
% end