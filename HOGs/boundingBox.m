function [Box] = boundingBox(I)
    %I = imread([file.folder, '/', file.name]);
    I2 = imresize(I, [30, 40]);
    I2 = rgb2gray(I2);
    % I2 = medfilt2(I2, [9 9]);
    I2 = imbinarize(I2);
    % I2 = medfilt2(I2, [5 5]);
    % [E, ~, Gx, Gy] = edge(I2, 'Sobel');
    % G = sqrt(double(Gx .* Gx + Gy .* Gy));
    
    for i=5:5:40
        [centers, radii, ~] = imfindcircles(I2, [i 3*i], ...
            'ObjectPolarity', 'bright');
        if ~(isempty(centers))
            break
        end
    end
    if isempty(centers)
        Box = [0, 0; 0, 0];
    else
        pmin = 108 * (centers(1,:) - radii(1));
        pmax = 108 * (centers(1,:) + radii(1));
        Box = [pmin; pmax];
    end
end