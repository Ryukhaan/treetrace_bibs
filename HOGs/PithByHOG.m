function [image, vizu, rows, cols] = PithByHOG(I, r, g, b, s, cs, cpb, acc)
% Convert image into grayscale
image = r * I(:,:,1) + g * I(:,:,2) + b * I(:,:,3);

% Apply gaussian filter
image = imgaussfilt(image, s);

% Parameters
% Window size
[h, w] = size(image);
% Cell size
cell_size = [cs, cs];
% Number of cells in y-axis and x-axis
num_cells = [h / cell_size(1), w / cell_size(2)];
% Total of cells
n_subimages = num_cells(1) * num_cells(2);
% Num bins
bins = 12;
% Number of cells per bloc (x-axis and y-axis)
bx = cpb;
by = cpb;
tic;
% Extract HOGs (Kurdthongmee et al. 2018)
[features, vizu] = extractHOGFeatures(image, ...
    'CellSize', cell_size, ...
    'NumBins', bins, ...
    'BlockSize', [by, bx], ...
    'BlockOverlap', [0 0], ...
    'UseSignedOrientation', true);

% Display image
% figure;
% imshow(I);
% hold on;
% plot(visu);

% Reshape HOGs like an n_subimages x bins matrix
hogs = zeros(bins, n_subimages);
%hogs = reshape(features, bins, n_subimages);
p = 1;
x = 1;
y = 1;
accy = 0;
accx = 0;
accb = 1;
for j=1:n_subimages
    i = (x-1) * num_cells(1) + y;
    hogs(:,i) = features(p:p+bins-1);
    p = p + bins;
    
    y = y + 1;
    accy = accy + 1;
    % Switch column cell-axis
    if mod(accy, by) == 0
        x = x + 1;
        y = y - by;
        accx = accx + 1;
        accy = 0;
    end
    % Switch bloc
    if mod(accx, bx) == 0 && mod(accy, by) == 0
        y = y + by;
        x = x - bx;
        accb = accb + 1;
        accx = 0;
    end
    % Switch column bloc-axis
    if mod(accb, num_cells(1)/by+1) == 0
        y = 1;
        x = x + bx;
        accb = 1;
    end
end

% All direction allowed
D = zeros(1, 4);
% Save [a, b] from line equation (y = ax + b)
droites = [];
% All angles available
angles = 360 - [110, 150, 190, 230, 270, 310, 350, 30, 70] + 90;
%angles = 360 - [75, 105, 135, 165, 195, 225, 255, 285, 315, 345, 15, 45] + 90;
% angles = 360 / bins .* (1:bins);

% Cell's middle (in pixels)
dx = cell_size(2) / 2;
dy = cell_size(1) / 2;
% Image's middle (in cells)
halfx = num_cells(2) / 2;
halfy = num_cells(1) / 2;

X = [];
Y = [];
for i=1:n_subimages
    [Fval, Farg] = max(hogs(:, i));
    x = floor(i / num_cells(1)) + 1;
    y = mod(i-1, num_cells(1)) + 1;
       % Top left
        if x < halfx && y < halfy
            %D = [4,5,6,10,11,12];
            D = [8, 9, 3, 4];
        end
        % Top right
        if x >= halfx && y < halfy
            %D = [1,2,3,7,8,9];
            D = [2, 3, 6, 7];
        end
        % Bottom left
        if x < halfx && y >= halfy
            %D = [1,2,3,7,8,9];
            D = [1, 2, 6, 7];
        end
        % Bottom right
        if x >= halfx && y >= halfy
            %D = [4,5,6,10,11,12];
            D = [8, 9, 3, 4];
        end
    
    %For all line with same F
    all_w = sum(exp(hogs(:, i)));
    for k=1:bins
        % Legacy code
        if (ismember(k, D)) && (hogs(k, i) == Fval)
        %if hogs(k,i) == Fval
            p = exp(hogs(k, i)) / all_w;
            
            % Retrieve angle and take the perpendicular to that one
            angle = angles(k);
            % Equation line : y = ax + b
            % Point-slope view given a point (a,b) and theta : y = tan(theta) (x-a) + b
            % Applying in our case : y = tan(theta) (x-dx) + dy
            % y = tan(theta).x + dy - tan(theta).dx
            a = tan( angle * pi / 180 );
            b = dy - a * dx;
            % Save (a, b)
            droites = [droites, [a, b, p]];
            %             X = [X, dx];
            %             Y = [Y, dy];
        end
    end
    % Increment cell's center
    dy = dy + cell_size(1);
    if mod(i, num_cells(1)) == 0
        dx = dx + cell_size(2);
        dy = cell_size(1) / 2;
    end
end

%Acc = zeros(h, w);
if acc
    Acc = line_acc(droites, h, w);
else
    Acc = line_intersec(droites, h, w);
end
%max(Acc(:))
%Acc = imquantize(Acc,16);
[rows, cols] = find(Acc==max(Acc(:)));
%scatter(rows * quant, cols * quant, 64, 'y', 'filled');
toc;
end

function [A] = line_acc(D, h, w)
n_lines = size(D);
A = zeros(h, w);
for j=1:3:n_lines(2) % 3 weighting | 2 without weights
    iy = 1:h;
    % Legacy
%     a = D(j);
%     b = D(j+1);
    a = D(j);
    b = D(j+1);
    p = 1;%D(j+2);
    ix = round((iy - b) / a);
    negx = find(ix<1);
    iy(negx(:)) = [];
    ix(negx(:)) = [];
    posx = find(ix>w);
    iy(posx(:)) = [];
    ix(posx(:)) = [];
    indexes = iy + (ix-1) * h;
    A(indexes(:)) = A(indexes(:)) + p;
end
end

function [A] = line_intersec(D, h, w)
n_lines = size(D);
A = zeros(h, w);
for i=1:n_lines(2)/2
    for j=1:n_lines(2)/2
        if i ~= j
            a = D(2*i-1);
            b = D(2*i);
            c = D(2*j-1);
            d = D(2*j);
            if a == c
                continue
            end
            % (ix, iy) point where y=ax+b and y=cx+d intersects
            ix = (d - b) / (a - c);
            iy = (a * (d - b) / (a - c)) + b;
            % If (ix, iy) is on image
            if ix >= 1 && ix < w && iy >= 1 && iy < h
                %ix = floor(ix / quant) + 1;
                %iy = floor(iy / quant) + 1;
                ix = floor(ix)+1;
                iy = floor(iy)+1;
                A(iy, ix) =  A(iy, ix) + 1;
            end
        end
    end
end
end